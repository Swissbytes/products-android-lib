package ch.swissbytes.syscomappproducts.services

import ch.swissbytes.syscomappbase.appspecific.SyncInterface
import ch.swissbytes.syscomappbase.appspecific.hasFleetRole
import ch.swissbytes.syscomappbase.appspecific.hasSellerRole
import ch.swissbytes.syscomappbase.interfaces.SyscomBaseSync
import ch.swissbytes.syscomappproducts.api.RestProducts
import ch.swissbytes.syscomappproducts.base.extensions.realmPersist
import ch.swissbytes.syscomappproducts.realm.RealmProduct
import io.reactivex.Observable
import retrofit2.Response



class ProductSync(val input: SyncInterface): SyscomBaseSync() {

    private val rest: RestProducts = input.run { RestProducts.create(url, token, versionCode, appId, timeout, lastUpdate) }

    override val isDebug: Boolean
        get() = input.debug

    override fun onResponse(model: Any) {
        realmPersist(model, realm = RealmProduct.getDefaultInstance())
    }

    override val calls: Map<String, Observable<out Response<out Any>>> by lazy {

        if (!input.roles.hasSellerRole() && !input.roles.hasFleetRole()){
            // The user is only collector
            mapOf("customerList" to rest.customerList())
        } else {
            mapOf(
                "products" to rest.products(),
                "purposeTypes" to rest.purposeTypes(),
                "pointTypes" to rest.pointTypes(),
                "categories" to rest.categories(),
                "characteristics" to rest.characteristics(),
                "fleetStock" to rest.fleetStock(),
                "locationStock" to rest.locationStock(),
                "prices" to rest.prices(),
                "productPriceList" to rest.priceList(),
                "customerList" to rest.customerList(),
                "currency" to rest.currency(),
                "deliveryModesList" to rest.deliveryModesList(),
                "businessUnitList" to rest.businessUnitList(),
                "agencyList" to rest.agencyList(),
                "paymentConditionList" to rest.paymentConditionList(),
                "ruleTypeList" to rest.ruleTypeList(),
                "validationTypeList" to rest.validationTypeList(),
                "partyClassificationsList" to rest.partyClassificationsList(),
                "partyRoleRelList" to rest.partyRoleRelList(),
                "partyRoleList" to rest.partyRoleList(),
                "priceRulesList" to rest.priceRulesList(), // <- Pricing library
                "priceRuleTargetList" to rest.priceRuleTargetList(),
                "priceRuleOutcomeModeList" to rest.priceRuleOutcomeModeList(),
                "priceRuleOutcomeTypeList" to rest.priceRuleOutcomeTypeList(),
                "priceRuleStatusList" to rest.priceRuleStatusList()
            )
        }

    }


}
