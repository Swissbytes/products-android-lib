package ch.swissbytes.syscomappproducts.entities.pricing

import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class PartyCategoryRealm: RealmModel {
    @PrimaryKey
    var id: Long? = null
    var companyId: Int? = null
    var fromDate: Long? = null
    var name: String? = null
    var parentUuid: String? = null
    var partyCategoryType: PartyCategoryTypeRealm? = null
    var thruDate: Long? = null
    var typeUuid: String? = null
    var uuid: String? = null
}

@RealmClass
open class PartyCategoryTypeRealm: RealmModel {
    @PrimaryKey var id: Long? = null
    var companyId: Long? = null
    var internalCode: String? = null
    var name: String ? = null
    var parentUuid: String? = null
    var uuid: String? = null
}
        
