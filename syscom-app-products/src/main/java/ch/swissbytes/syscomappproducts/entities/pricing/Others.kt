package ch.swissbytes.syscomappproducts.entities.pricing

import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class DelAgencyRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var name: String? = null
    var code: String? = null

    //TODO migration realm

    //    var lastUpdate: Long? = null
//    var latitude: Long? = null
//    var longitude: Long? = null

    //    var address: String? = null

//    var phone: String? = null
//    var status: Long? = null
}

@RealmClass
open class RelDeliveryModeRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var name: String? = null

    // TODO  migrate
//    var label: String? = null
}

@RealmClass
open class RelPriceRulesPartyClassificationRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var name: String? = null
}

@RealmClass
open class RelGeographicBoundaryRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var name: String? = null
    var latitude: String? = null
    var longitude: String? = null
}

@RealmClass
open class RelCustomerRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var name: String? = null
}

@RealmClass
open class RelRulesTypeRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var label: String? = null
    var name: String? = null
}

@RealmClass
open class RelBusinessUnitRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var lastUpdate: Long? = null
    var name: String? = null
    var status: Long? = null
}

@RealmClass
open class RelPartyRolesRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var label: String? = null
    var name: String? = null
}

@RealmClass
open class RelPriceRuleTargetRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var label: String? = null
    var name: String? = null
}

@RealmClass
open class RelOutcomeModeRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var label: String? = null
    var name: String? = null
}

@RealmClass
open class RelOutcomeTypeRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var label: String? = null
    var name: String? = null
}

@RealmClass
open class RelPriceRuleStatusRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var label: String? = null
    var name: String? = null
}

@RealmClass
open class RelValidationTypeRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var label: String? = null
    var name: String? = null
}

