package ch.swissbytes.syscomappproducts.entities.prodPrice

import io.realm.RealmList
import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass


@RealmClass
open class ProductPriceListRealm: RealmModel {
    @PrimaryKey
    var id: Long? = null
    var companyId: Long? = null
    var currency: String? = null
    var default: Boolean? = null
    var enablePricing: Boolean? = null
    var from: Long? = null
    var to: Long? = null
    var name: String? = null
    var parties: RealmList<PriceListPartyRealm> = RealmList()
    var productPrices: RealmList<PriceListProductPriceRealm> = RealmList()
    var status: String? = null
}

@RealmClass
open class PriceListProductPriceRealm: RealmModel {
    @PrimaryKey
    var productId: Long? = null
    var price: String? = null
}

@RealmClass
open class PriceListPartyRealm: RealmModel{
    @PrimaryKey
    var partyId: Long? = null
    var priority: Long? = null
}