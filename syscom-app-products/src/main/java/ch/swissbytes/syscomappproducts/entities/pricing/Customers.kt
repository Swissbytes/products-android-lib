package ch.swissbytes.syscomappproducts.entities.pricing

import androidx.annotation.StringRes
import ch.swissbytes.syscomappproducts.R
import ch.swissbytes.syscomappproducts.base.annotations.ExcludeGson
import io.realm.RealmList
import io.realm.RealmModel
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class CustomerRealm: RealmModel {
    @PrimaryKey
    var id: String? = null
    var partyId: String? = null
    var name: String? = null
    var nit: String? = null
    var contractExpiration: Long? = null
    var credit: String? = null
    var geographicBoundaries: RealmList<GeographicBoundaryRealm> = RealmList()
    var internalCode: String ? = null
    var invoiceName: String ? = null
    var partyClassifications: RealmList<CustomerPartyClassificationRealm> = RealmList()
    var contactMechanisms: RealmList<PointRealm> = RealmList()
    var status: String? = null
    var businessUnitId: Long? = null
    var agencyId: Long? = null
    var regimenType: String? = null
    var docNumber: String? = null

    var priceLists: RealmList<PriceListDetailRealm> = RealmList()


    fun findPriceListWithGreaterPriorityByPriceListId() = priceLists.maxBy { it.priority ?: 0 }


    @ExcludeGson var inSyncWithServer:Boolean = true
    @ExcludeGson var originalAppUuid:String? = null

    @Ignore @ExcludeGson
    var deliveryPoints: List<PointRealm>? = null
    get() = contactMechanisms.filter { c ->  c.type?.type == PointType.POSTAL_ADDRESS.name &&
            c.purposes.firstOrNull { it.type == ContactMechanismPurposeType.DELIVERY_POINT.name } != null }

    @Ignore @ExcludeGson
    var payPoints: List<PointRealm>? = null
        get() = contactMechanisms.filter{ c -> c.type?.type == PointType.POSTAL_ADDRESS.name &&
            c.purposes.firstOrNull { it.type == ContactMechanismPurposeType.PAY_POINT.name } != null
        }

    override fun toString(): String =
        "id: $id" +
        "partyId: $partyId" +
        "name: $name" +
        "nit: $nit" +
        "contractExpiration: $contractExpiration" +
        "credit: $credit" +
        "internalCode: $internalCode" +
        "invoiceName: $invoiceName" +
        "status: $status" +
        "businessUnitId: $businessUnitId" +
        "agencyId: $agencyId" +
        "regimenType: $regimenType"
}

@RealmClass
open class PriceListDetailRealm: RealmModel {
    @PrimaryKey
    var id: Long? = null
    var priceListId: Long? = null
    var priority: Long? = null
}

@RealmClass
open class ProdCurrencyRealm: RealmModel {
    @PrimaryKey var id: Long = 0L
    var quote: String? = null
}

@RealmClass
open class PartyRolesRealm: RealmModel {
    @PrimaryKey
    var id: Long? = null
    var partyId: Long? = null
    var name: String? = null
    var status: String? = null
}

enum class ContactMechanismPurposeType(@StringRes val res: Int ){
    DELIVERY_POINT(R.string.delivery_point_label), PAY_POINT(R.string.pay_point_label)
}

@RealmClass
open class PointRealm: RealmModel {
    @PrimaryKey
    var id: String? = null
    var type: PointTypeRealm? = null
    var customerId: String? = null
    var name: String? = null
    var cityId: Long? = null
    var neighborhood: String? = null
    var avenueStreet: String? = null
    var number: String? = null
    var flat: String? = null
    var reference: String? = null
    var uv: String? = null
    var block: String? = null
    var purposes: RealmList<PurposeTypeRealm> = RealmList()
    var latitude: String? = null
    var longitude: String? = null
    var value: String? = null

    @ExcludeGson var inSyncWithServer:Boolean = true
    @ExcludeGson var originalAppUuid:String? = null
}

@RealmClass
open class PurposeTypeRealm: RealmModel {
    @PrimaryKey
    var id: Long? = null
    var name: String? = null
    var type: String? = null
}


@RealmClass
open class PointTypeRealm: RealmModel {
    @PrimaryKey
    var id: String? = null
    var name: String? = null
    var type: String? = null
}

enum class PointType {
    POSTAL_ADDRESS,
    PHONE_NUMBER,
    EMAIL,
    WEBSITE,
    OTHER
}

@RealmClass
open class GeographicBoundaryRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var code: String? = null
    var latitude: String? = null
    var longitude: String? = null
    var name: String? = null
    var type: String? = null
}

@RealmClass
open class CustomerPartyClassificationRealm: RealmModel {
    @PrimaryKey
    var id: Long? = null
    var name: String? = null
}


@RealmClass
open class CustomerPartyRoleRealm: RealmModel {
    @PrimaryKey
    var id: Long? = null
    var label: String? = null
    var name: String? = null
}