package ch.swissbytes.syscomappproducts.entities

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.math.BigDecimal


interface SaleProduct {
    var prodId: Long?
    var name: String?
    var qty: BigDecimal?
    var unitPrice: String?
    var discountPercentage: String?
    var trackingType: String?
    var unitMeasureId: Long?
    var mustControlledOnSale: Boolean?
    var hasWarranty: Boolean?

    var addedByPricingLibType: Long?
    var totalPrice: String?
}

interface SaleProductParcelable: SaleProduct, Parcelable {
    fun string(): String =
                "prodId: $prodId " +
                "qty: $qty " +
                "unitPrice: $unitPrice " +
                "totalPrice(withDiscount): $totalPrice " +
                "discountPercentage: $discountPercentage"
}

@Parcelize
data class SaleProductConcrete(
    override var prodId: Long? = null,
    override var name: String? = null,
    override var qty: BigDecimal? = null,
    override var unitPrice: String? = null,
    override var discountPercentage: String? = null,
    override var totalPrice: String? = null,
    override var trackingType: String? = null,
    override var unitMeasureId: Long? = null,
    override var mustControlledOnSale: Boolean? = null,
    override var hasWarranty: Boolean? = null,
    override var addedByPricingLibType: Long? = null
): SaleProductParcelable