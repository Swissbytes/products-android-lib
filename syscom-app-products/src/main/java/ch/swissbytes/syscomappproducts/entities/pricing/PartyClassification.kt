package ch.swissbytes.syscomappproducts.entities.pricing

import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

@RealmClass
open class PartyClassificationRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var company: CompanyRealm? = null
    var fromDate: Long? = null
    var name: String? = null
    var parentUuid: String? = null
    var partyCategoryType: PartyCategoryTypeRealm? = null
    var thruDate: Long? = null
    var uuid: String? = null
}

@RealmClass 
open class CompanyRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var activateRequest: Boolean? = null
    var address: String? = null
    var allowShowRoomSaleWithCredit: Boolean? = null
    var banner: String? = null
    var city: String? = null
    var code: String? = null
    var completeCode: String? = null
    var currency: String? = null
    var logo: String? = null
    var logoReport: String? = null
    var name: String? = null
    var new: Boolean? = null
    var nit: Long? = null
    var phone: String? = null
}
