package ch.swissbytes.syscomappproducts.entities.pricing

data class Type(
    val id: Int,
    val name: String,
    val type: String
)