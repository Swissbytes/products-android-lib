package ch.swissbytes.syscomappproducts.entities

import ch.swissbytes.syscomappbase.extensions.toBigDecimalOrZero
import ch.swissbytes.syscomappproducts.base.extensions.realmGetItemCopy
import ch.swissbytes.syscomappproducts.realm.RealmProduct
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmModel
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass
import java.math.BigDecimal

interface Namable{
    var id: Long?
    var name: String?
    fun isLeaf(): Boolean = false
}

enum class TrackingType{ NONE, SERIAL, LOT }

@RealmClass
open class ProductRealm: RealmModel, Namable {

    companion object { val CHARACTERISTICS = "characteristics.value"
        val B_IDS = "businessUnitIds.value"
        val PATH = "path.value"
        val NAME = "name"
        val ID = "id" }

    @PrimaryKey override var id: Long? = null
    override var name: String? = null
    var code: String? = null

    var companyId: Long? = null
    var trackingType: String? = null

    var path: RealmList<RealmLong> = RealmList()
    var characteristics: RealmList<RealmLong> = RealmList()
    var businessUnitIds: RealmList<RealmLong> = RealmList()

    var mustControlledOnSale: Boolean? = null
    var hasWarranty: Boolean? = null
    var policyId: Long? = null
    //Warehouse ids

    var unitMeasures: RealmList<ProductUnitMeasureRealm> = RealmList()
    var productClassifications: RealmList<ProductClassificationRealm> = RealmList()

    fun hasSerial(): Boolean = if (trackingType == null)  false else  trackingType!! != TrackingType.NONE.name

    fun getStockByWarehouseId(wid: Long): ProductStockRealm? =
        realmGetItemCopy(ProductStockRealm::class.java, realm = RealmProduct.getDefaultInstance()) {
            equalTo(ProductStockRealm.P_ID, this@ProductRealm.id).equalTo(ProductStockRealm.W_ID, wid)
        }

    fun getStockAvailableByWarehouseId(wid: Long): ProductStockRealm? =
        getStockByWarehouseId(wid)?.takeIf { it.qtyAsBigDecimal > BigDecimal.ZERO  }

    override fun toString(): String = "id: $id, name: $name"

}

@RealmClass
open class ProductClassificationRealm: RealmModel {
    @PrimaryKey var id: Long? = null
    var name: String? = null
    var path: String? = null
}

@RealmClass
open class ProductUnitMeasureRealm: RealmModel {
    @PrimaryKey var id: Long? = null
    var name: String? = null
    var abbrevation: String? = null
}



@RealmClass
open class ProductStockRealm: QtyAsBigDecimal, RealmModel {

    companion object {
        val P_ID = "prodId"
        val W_ID = "warehouseId"
        val AVAILABLE = "available"
    }

    @PrimaryKey
    var id: String? = null
    var prodId: Long? = null
    var prodName: String? = null

    var unitMeasureId: Long? = null
    var available: String? = null
    var warehouseId: Long? = null

    var reserved: Long? = null

    @Ignore
    override var qty: String? = "0"
        get() = available.toString()

    @SerializedName(value = "lots", alternate = ["productLotMobileStoreDtos"] )
    var lots: RealmList<ProductStockLotRealm> = RealmList()

}

//        "id": 3,
//        "prodId": 12194,
//        "prodName": "SET 3 TUPPER CUADRADO IML 390/690/1200 ML (12 JGO) PQT CALIPSO JAGUAR",
//        "available": "1.00000",
//        "warehouseId": 18,
//        "productLotMobileStoreDtos": [
//        {
//            "id": 1,
//            "qty": "1.00000",
//            "ref": "MPD-098723"
//        }
//        ]

interface QtyAsBigDecimal{

    var qty: String?

    var qtyAsBigDecimal: BigDecimal set(v) {} get() = qty.toBigDecimalOrZero().stripTrailingZeros()
}

@RealmClass
open class ProductStockLotRealm: QtyAsBigDecimal, RealmModel{
    @PrimaryKey var id: String? = null
    var ref: String? = null
    override var qty: String? = null
}

@RealmClass
open class ProductPriceRealm: RealmModel {

    companion object {
        val B_ID = "businessUnitId"
        val P_ID = "productId"
    }

    @PrimaryKey
    var id: Long? = null

    var businessUnitId: Long? = null
    var productId: Long? = null
    var price: String? = null

    //TODO add realm migration
    var priceListId: Long? = null
}

@RealmClass
open class CategoryRealm: RealmModel, Namable {

    companion object { val PARENT_ID = "parentId"
        val ID = "id" }

    @PrimaryKey override var id: Long? = null
    override var name: String? = null
    var parentId: Long? = null
    var leaf: Boolean = false

    override fun isLeaf() = leaf
}

@RealmClass
open class ProductFeatureValueRealm: RealmModel, Namable {

    companion object { val ID = "id"
        val PROD_FEATURE_ID = "productFeature.id"
        val NAME = "name" }

    @PrimaryKey override var id: Long? = null
    override var name: String? = null
    var productFeature: ProductFeatureRealm? = null
}

@RealmClass
open class ProductFeatureRealm: RealmModel, Namable {

    companion object { val PROD_FEATURE_ID = "id"
        val PROD_FEATURE_NAME = "name" }

    @PrimaryKey override var id: Long? = null
    override var name: String? = null
    var status: String? = null
}

@RealmClass
open class RealmLong: RealmModel{
    @PrimaryKey var value: Long? = null
}