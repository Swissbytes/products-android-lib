package ch.swissbytes.syscomappproducts.entities.pricing

import io.realm.RealmList
import io.realm.RealmModel
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass


@RealmClass
open class PriceRulesRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var name: String? = null
    var status: String? = null
    var companyId: Long? = null
    var description: String? = null
    var accumulative: Boolean? = null
    var fromDate: Long? = null
    var thruDate: Long? = null
    var priority: Long? = null
    var useSaleOrderTotalForOutcome: Boolean? = null
    var ruleType: String? = null
    var outcomeMode: String? = null
    var outcomeType: String? = null
    var validationType: String? = null
    var target: String? = null
    var paymentCondition: String? = null

    var businessUnits: RealmList<RelBusinessUnitRealm> = RealmList()
    var agencies: RealmList<DelAgencyRealm> = RealmList()
    var deliveryModes: RealmList<RelDeliveryModeRealm> = RealmList()        // ??
    var partyClassifications: RealmList<RelPriceRulesPartyClassificationRealm> = RealmList()
    var geographicBoundaryList: RealmList<RelGeographicBoundaryRealm> = RealmList()
    var customerList: RealmList<RelCustomerRealm> = RealmList()             // ??
    var roleTypeList: RealmList<PriceRuleRoleTypeRealm> = RealmList()
    var products: RealmList<PriceRulesProductRealm> = RealmList()
    var productFeatures: RealmList<PriceRulesProductFeatureRealm> = RealmList()
    var productClassifications: RealmList<PriceRulesProductClassificationRealm> = RealmList()

    var output: OutputRealm? = null
}

@RealmClass
open class RelPaymentConditionRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var label: String? = null
    var name: String? = null
}

@RealmClass
open class PriceRulesProductClassificationRealm: RealmModel {
    @PrimaryKey
    var id: Long? = null
    var path: String? = null
    var name: String? = null
}

@RealmClass
open class PriceRuleRoleTypeRealm: RealmModel {
    @PrimaryKey
    var id: Long? = null
    var label: String? = null
    var name: String? = null
}

@RealmClass
open class PriceRulesProductFeatureRealm: RealmModel {
    @PrimaryKey
    var id: Long? = null
    var featureId: Long? = null
    var featureValueId: Long? = null
    var name: String? = null
    var value: String? = null
}

@RealmClass
open class OutputRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var outcomeMode: String? = null
    var validationType: String? = null
    var frequency: String? = null
    var outcomeType: String? = null
    var outcome: OutcomeRealm? = null
    var scales: RealmList<ScaleRealm> = RealmList()
}

@RealmClass
open class ScaleRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var from: String? = null
    var thru: String? = null
    var type: String? = null
    var outcome: OutcomeRealm? = null
}

@RealmClass
open class OutcomeRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var outcomeType: String? = null
    var discountAmount: String? = null
    var discountPercentage: String? = null
    var surchargeAmount: String? = null
    var surchargePercentage: String? = null
    var fixedPrice: String? = null
    var products: RealmList<PriceRulesProductRealm> = RealmList()
}

@RealmClass
open class PriceRulesProductRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var code: String? = null
    var name: String? = null
    var unitMeasure: UnitMeasureRealm? = null

    var qty: Int? = null
}

@RealmClass
open class UnitMeasureRealm: RealmModel{
    @PrimaryKey
    var id: Long? = null
    var abbreviation: String? = null
    var name: String? = null
}