package ch.swissbytes.syscomappproducts.pricing


import android.util.Log
import ch.swissbytes.pricinglib.enums.*
import ch.swissbytes.pricinglib.factors.*
import ch.swissbytes.pricinglib.inputs.PriceComponentRuleInputSaleOrder
import ch.swissbytes.pricinglib.inputs.PriceComponentRuleInputSaleOrderProduct
import ch.swissbytes.pricinglib.models.PriceComponentRule
import ch.swissbytes.pricinglib.models.PriceRule
import ch.swissbytes.pricinglib.outcomes.PriceComponentRuleOutcome
import ch.swissbytes.pricinglib.outcomes.PriceComponentRuleOutcomeOutput
import ch.swissbytes.pricinglib.outcomes.PriceComponentRuleOutcomeProduct
import ch.swissbytes.pricinglib.subjects.PriceComponentRuleSubjectProduct
import ch.swissbytes.pricinglib.subjects.PriceComponentRuleSubjectProductFeature
import ch.swissbytes.pricinglib.subjects.PriceComponentRuleUnitMeasure
import ch.swissbytes.syscomappproducts.base.extensions.realmGetItemCopy
import ch.swissbytes.syscomappproducts.base.extensions.realmGetOrderedList
import ch.swissbytes.syscomappproducts.daoandservices.PriceDao
import ch.swissbytes.syscomappproducts.entities.pricing.CustomerRealm
import ch.swissbytes.syscomappproducts.entities.pricing.PartyRolesRealm
import ch.swissbytes.syscomappproducts.entities.pricing.PriceRulesRealm
import ch.swissbytes.syscomappproducts.realm.RealmProduct
import java.math.BigDecimal
import java.util.*

enum class PricingEntityStatus {
    ACTIVE, DISABLED
}

class Pricing(val priceDao: PriceDao) {

    private val TAG = this::class.java.simpleName

    fun computeOrder(
        saleOrder: PriceComponentRuleInputSaleOrder
    ): List<PriceComponentRuleInputSaleOrderProduct> {

        val start = System.currentTimeMillis()
        Log.i(TAG, "fetchPriceRuleList start")
        val priceRuleList = fetchPriceRuleList(saleOrder.businessUnit.id, saleOrder.customer.id)
        val end = System.currentTimeMillis()
        Log.i(TAG, "fetchPriceRuleList took ${end - start}")

        val rules = PriceComponentRule(priceRuleList, PriceRuleResolutionModeEnum.BY_BEST_VALUE_SALE_ORDER, false)
        val response = rules.computate(saleOrder)

        return response
    }

    private fun fetchPriceRuleList(
        bId: Long,
        customerId: Long
    ): List<PriceRule> {

        val (realm, rules) = realmGetOrderedList(
            PriceRulesRealm::class.java,
            realm = RealmProduct.getDefaultInstance()
        ) { it.equalTo("status", PricingEntityStatus.ACTIVE.name) }


        val list = rules.map { rule ->
            PriceRule().apply {

                id = rule.id!!
                name = rule.name
                description = rule.description
                rule.fromDate?.let { fromDate = Date(it) }
                rule.thruDate?.let { thruDate = Date(it) }
                rule.accumulative?.let { isAccumulative = it }
                rule.priority?.let { priority = it }
                rule.ruleType?.let { ruleType = PriceRuleTypeEnum.valueOf(it) }
                rule.target?.let { target = TargetEnum.valueOf(it) }
                rule.outcomeMode?.let { outComeMode = OutComeModeEnum.valueOf(it) }
                rule.validationType?.let { validationType = ScaleTypeEnum.valueOf(it) }
                rule.useSaleOrderTotalForOutcome?.let { isUseSaleOrderTotalForOutcome = it }
                val ruleOutput = rule.output!!

                output = PriceComponentRuleOutcomeOutput.builder()
                    .id(ruleOutput.id!!)
                    .outComeModeEnum(ruleOutput.outcomeMode?.let { OutComeModeEnum.valueOf(it) })
                    .validationType(ruleOutput.validationType?.let { ScaleTypeEnum.valueOf(it) })
                    .scales(ruleOutput.scales.map { scale ->

                        PriceComponentRuleFactorScale().apply {
                            id = scale.id!!
                            from = scale.from?.toBigDecimal()
                            thru = scale.thru?.toBigDecimal()
                            type = ScaleTypeEnum.valueOf(scale.type!!)
                            //                            private ScaleTypeEnum type;
                            val scaleOutcome = scale.outcome!!
                            outcome = PriceComponentRuleOutcome().apply {
                                scaleOutcome.id?.let { id = it }
                                scaleOutcome.outcomeType?.let {
                                    outcomeType = PriceComponentOutcomeTypeEnum.valueOf(it)
                                }
                                discountAmount = scaleOutcome.discountAmount?.toBigDecimal()
                                discountPercentage = scaleOutcome.discountPercentage?.toBigDecimal()
                                surchargeAmount = scaleOutcome.surchargeAmount?.toBigDecimal()
                                surchargePercentage = scaleOutcome.surchargePercentage?.toBigDecimal()
                                fixedPrice = scaleOutcome.fixedPrice?.toBigDecimal()
                                products = scaleOutcome.products.map { pr ->
                                    PriceComponentRuleOutcomeProduct().apply {
                                        pr.id?.let { id = it }
                                        pr.code?.let { code = it }
                                        pr.name?.let { name = it }

                                        //TODO
//                                        pr.id?.let { originalPrice = priceDao.getProductPriceWithRealm(realm, it, bId, customerId) }
                                        pr.id?.let { originalPrice = BigDecimal.ZERO }

                                        pr.unitMeasure?.let { pmeasure ->
                                            unitMeasure = PriceComponentRuleUnitMeasure().apply {
                                                pmeasure.id?.let { id = it }
                                                pmeasure.name?.let { name = it }
                                                pmeasure.abbreviation?.let { abbreviation = it }
                                            }
                                        }
                                        qty = pr.qty!!
                                    }
                                }
                            }
                        }
                    })
                    .frequency(ruleOutput.frequency?.toBigDecimal())
                    .outcomeType(ruleOutput.outcomeType?.let { PriceComponentOutcomeTypeEnum.valueOf(it) })
                    .outcome(PriceComponentRuleOutcome().apply {
                        ruleOutput.outcome?.let { ruleOutcome ->
                            ruleOutcome.id?.let { id = it }
                            ruleOutcome.outcomeType?.let { outcomeType = PriceComponentOutcomeTypeEnum.valueOf(it) }
                            ruleOutcome.discountAmount?.let { discountAmount = it.toBigDecimal() }
                            ruleOutcome.discountPercentage?.let { discountPercentage = it.toBigDecimal() }
                            ruleOutcome.surchargeAmount?.let { surchargeAmount = it.toBigDecimal() }
                            ruleOutcome.surchargePercentage?.let { surchargePercentage = it.toBigDecimal() }
                            ruleOutcome.fixedPrice?.let { fixedPrice = it.toBigDecimal() }
                            products = ruleOutcome.products.map { pr ->
                                PriceComponentRuleOutcomeProduct().apply {
                                    pr.id?.let { id = it }
                                    code = pr.code
                                    name = pr.name

                                    //TODO
//                                    pr.id?.let { originalPrice = priceDao.getProductPriceWithRealm(realm, it, bId, customerId) }
                                    pr.id?.let { originalPrice = BigDecimal.ZERO }

                                    pr.unitMeasure?.let { pmeasure ->
                                        unitMeasure = PriceComponentRuleUnitMeasure().apply {
                                            id = pmeasure.id!!
                                            name = pmeasure.name
                                            abbreviation = pmeasure.abbreviation
                                        }
                                    }
                                    qty = pr.qty!!
                                }
                            }
                        }
                    })
                    .outComeModeEnum(ruleOutput.outcomeMode?.let { OutComeModeEnum.valueOf(it) })
                    .validationType(ruleOutput.validationType?.let { ScaleTypeEnum.valueOf(it) })
                    .frequency(ruleOutput.frequency?.toBigDecimal())
                    .outcomeType(ruleOutput.outcomeType?.let { PriceComponentOutcomeTypeEnum.valueOf(it) })
                    .build()

                Log.i(TAG, "rule.products.map start")
                val start = System.currentTimeMillis()
                products = rule.products.map { pr ->
                    PriceComponentRuleSubjectProduct().apply {
                        pr.id?.let { id = it }
                        code = pr.code
                        name = pr.name

                        //TODO
//                        pr.id?.let { originalPrice = priceDao.getProductPriceWithRealm(realm, it, bId, customerId) }
                        pr.id?.let { originalPrice = BigDecimal.ZERO }

                        pr.unitMeasure?.let { pmeasure ->
                            unitMeasure = PriceComponentRuleUnitMeasure().apply {
                                id = pmeasure.id!!
                                name = pmeasure.name
                                abbreviation = pmeasure.abbreviation
                            }
                        }
                    }
                }
                val end = System.currentTimeMillis()
                Log.i(TAG, "rule.products.map took ${end - start}")

                productFeatures = rule.productFeatures.map { pr ->
                    PriceComponentRuleSubjectProductFeature().apply {
                        id =  pr.featureValueId!!
                        name = pr.name
                        value = pr.value
                    }
                }

                productClassifications = rule.productClassifications.map { pc ->
                    PriceComponentRuleFactorProductClassification().apply {
                        pc.id?.let { id = it }
                        name = pc.name
                        path = pc.path
                    }
                }

                paymentCondition = rule.paymentCondition?.let { PaymentConditionsTypeEnum.valueOf(it) }
                partyClassifications = rule.partyClassifications.map { pc ->
                    PriceComponentRuleFactorPartyClassification().apply {
                        pc.id?.let { id = it }
                        name = pc.name
                    }
                }
                geographicBoundaries = rule.geographicBoundaryList.map { gb ->
                    PriceComponentRuleFactorGeographicBoundary().apply {
                        gb.id?.let { id = it }
                        name = gb.name
                        latitude = gb.latitude?.toBigDecimal()
                        longitude = gb.longitude?.toBigDecimal()
                    }
                }

                customers = rule.customerList.map mmap@ { cust ->
                    val customer = realmGetItemCopy(
                        CustomerRealm::class.java,
                        realm = RealmProduct.getDefaultInstance()
                    ) { equalTo("id", cust.id!!.toString()) }

                    //TODO fetch by partyId wait for pricing lib update 25 Octobre 2019
                    if (customer == null){
                        Log.i(TAG, "customer not found ${cust.id}")
                        return@mmap null
                    }

                    val partyClassifications = customer.partyClassifications.map { pc ->
                        PriceComponentRuleFactorPartyClassification().apply {
                            pc.id?.let { id = it }
                            name = pc.name
                        }
                    }

                    val geographicBoundaries = customer.geographicBoundaries.map { pc ->
                        PriceComponentRuleFactorGeographicBoundary().apply {
                            pc.id?.let { id = it }
                            name = pc.name
                            latitude = pc.latitude?.toBigDecimal()
                            longitude = pc.longitude?.toBigDecimal()
                        }
                    }

                    val roles = customer.let {
                        realm.where(PartyRolesRealm::class.java)
                                //TODO check what happen if null
                            .equalTo("partyId", it.partyId?.toLong())
                            .equalTo("status", "ENABLE")
                            .findAll()
                            .map { pr -> RoleTypeEnum.valueOf(pr.name!!) }
                    }


                    PriceComponentRuleFactorCustomer.builder()
                        .id(customer.id!!.toLong())
                        .roles(roles)
                        .partyClassifications(partyClassifications)
                        .geographicBoundaries(geographicBoundaries)
                        .build()

                }.filterNotNull()


                agencies = rule.agencies.map { ag ->
                    PriceComponentRuleFactorAgency(0, null).apply {
                        ag.id?.let { id = it }
                        name = ag.name
                    }
                }

                businessUnits = rule.businessUnits.map { bu ->
                    PriceComponentRuleFactorBusinessUnit().apply {
                        bu.id?.let { id = it }
                        name = bu.name
                    }
                }

                roleTypes = rule.roleTypeList.map { rt ->
                    PriceComponentRuleFactorRoleType().apply {
                        rt.id?.let { id = it }
                        roleType = rt.name?.let { RoleTypeEnum.valueOf(it) }
                    }
                }
            }
        }
        realm.close()
        return list
    }

}
