package ch.swissbytes.syscomappproducts.api

import ch.swissbytes.syscomappproducts.entities.*
import ch.swissbytes.syscomappproducts.entities.pricing.*
import ch.swissbytes.syscomappproducts.entities.prodPrice.ProductPriceListRealm
import io.reactivex.Observable
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import java.io.IOException
import java.util.concurrent.TimeUnit
import javax.annotation.ParametersAreNonnullByDefault

interface RestProducts {

    @GET("products")
    fun products(): Observable<Response<List<ProductRealm>>>

    @GET("categories")
    fun categories(): Observable<Response<List<CategoryRealm>>>

    @GET("characteristics")
    fun characteristics(): Observable<Response<List<ProductFeatureValueRealm>>>

    @GET("productprices")
    fun prices(): Observable<Response<List<ProductPriceRealm>>>

    @GET("shared/priceList/list")
    fun priceList(): Observable<Response<List<ProductPriceListRealm>>>

    @GET("mobilestore/fleet/stock")
    fun fleetStock(): Observable<Response<List<ProductStockRealm>>>

//    esta ya lo tenemos en scv:
//    /rest/mobile/shared/location/stock -> Stock sin fleet
//
//    Y este es el del fleet:
//    /rest/mobile/mobilestore/fleet/stock

    @GET("shared/location/stock")
    fun locationStock(): Observable<Response<List<ProductStockRealm>>>

    @GET("priceRule/list")
    fun priceRulesList(): Observable<Response<List<PriceRulesRealm>>>

    @GET("partyRole/list")
    fun partyRoleList(): Observable<Response<List<PartyRolesRealm>>>

    @GET("partyRoleRel/list")
    fun partyRoleRelList(): Observable<Response<List<RelPartyRolesRealm>>>

    @GET("customer/list")
    fun customerList(): Observable<Response<List<CustomerRealm>>>

    @GET("mobilestore/exchangeRate")
    fun currency(): Observable<Response<ProdCurrencyRealm>>

    @GET("deliveryModes/list")
    fun deliveryModesList(): Observable<Response<List<RelDeliveryModeRealm>>>

    @GET("business-unit/list")
    fun businessUnitList(): Observable<Response<List<RelBusinessUnitRealm>>>

    @GET("agency/list")
    fun agencyList(): Observable<Response<List<DelAgencyRealm>>>

    @GET("payment-condition/list")
    fun paymentConditionList(): Observable<Response<List<RelPaymentConditionRealm>>>

    @GET("priceRule/ruleType/list")
    fun ruleTypeList(): Observable<Response<List<RelRulesTypeRealm>>>

    @GET("priceRule/validationType/list")
    fun validationTypeList(): Observable<Response<List<RelValidationTypeRealm>>>

    @GET("priceRule/target/list")
    fun priceRuleTargetList(): Observable<Response<List<RelPriceRuleTargetRealm>>>

    @GET("priceRule/outcomeMode/list")
    fun priceRuleOutcomeModeList(): Observable<Response<List<RelOutcomeModeRealm>>>

    @GET("priceRule/outcomeType/list")
    fun priceRuleOutcomeTypeList(): Observable<Response<List<RelOutcomeTypeRealm>>>

    @GET("priceRule/status/list")
    fun priceRuleStatusList(): Observable<Response<List<RelPriceRuleStatusRealm>>>

    @GET("partyClassifications/list")
    fun partyClassificationsList(): Observable<Response<List<PartyClassificationRealm>>>

    @GET("shared/contactMechanism/purposeTypes")
    fun purposeTypes(): Observable<Response<List<PurposeTypeRealm>>>

    @GET("shared/contactMechanism/types")
    fun pointTypes(): Observable<Response<List<PointTypeRealm>>>


    companion object {
        fun create(url:String, token: String, versionCode: String, appId: String, timeout: Long = 15L, lastUpdate: Long? = null): RestProducts {
            val client = OkHttpClient().newBuilder()
            client.interceptors().add(HeaderInterceptor(token, versionCode, appId, lastUpdate))
            client.readTimeout(timeout, TimeUnit.SECONDS)

//            if (BuildConfig.DEBUG){
//                val interceptor = HttpLoggingInterceptor()
//                interceptor.level = HttpLoggingInterceptor.Level.BODY
//                client.interceptors().add(interceptor)
//            }

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client.build())
                    .baseUrl(url)
                    .build()

            return retrofit.create(RestProducts::class.java)
        }
    }

}

class HeaderInterceptor(
    private val token: String,
    private val versionCode: String,
    private val appId: String,
    private val lastUpdate: Long?
) : Interceptor {
    private val AUTHORIZATION = "Authorization"
    private val BUILD = "versionAppBuild"
    private val CODE = "versionAppCode"
    private val LAST_UPDATE = "lastUpdate"

    @ParametersAreNonnullByDefault
    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
        val requestBuilder = chain.request().newBuilder()
        requestBuilder.addHeader(AUTHORIZATION, "Bearer $token")
        requestBuilder.addHeader(BUILD, versionCode)
        requestBuilder.addHeader(CODE, appId)
        requestBuilder.addHeader(LAST_UPDATE, "${lastUpdate ?: 0}")
        return chain.proceed(requestBuilder.build())
    }
}