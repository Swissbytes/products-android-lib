package ch.swissbytes.syscomappproducts.prefs

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager

@SuppressWarnings("all")
class AppPref(context: Context) {

    companion object {
        val USER_NAME: String = "username"
        val PASSWORD: String = "password"
    }

    val mSharedPreferences: SharedPreferences by lazy {
        PreferenceManager.getDefaultSharedPreferences(context)
    }

    fun setUserName(userName: String) {
        mSharedPreferences.edit().putString(USER_NAME, userName).commit()
    }

    fun getUserName(): String? {
        return mSharedPreferences.getString(USER_NAME, null)
    }

    fun setPassword(pass: String) {
        mSharedPreferences.edit().putString(PASSWORD, pass).commit()
    }

    fun getPassword(): String? {
        return mSharedPreferences.getString(PASSWORD, null)
    }

}