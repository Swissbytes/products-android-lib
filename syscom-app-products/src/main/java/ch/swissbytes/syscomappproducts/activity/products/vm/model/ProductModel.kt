package ch.swissbytes.syscomappproducts.activity.products.vm.model

import ch.swissbytes.syscomappproducts.entities.CategoryRealm
import ch.swissbytes.syscomappproducts.entities.Namable
import ch.swissbytes.syscomappproducts.entities.ProductFeatureRealm
import ch.swissbytes.syscomappproducts.entities.ProductRealm
import java.util.*
import java.util.Collections.emptyList

class ProductModel: Observable() {

    companion object {
        val PRODUCTS = "products"
        val CATEGORIES = "categories"
        val PRODUCT_FEATURES = "productFeatures"
        val PRODUCT_FEATURE_VALUE_SELECTED = "productFeatureValuesSelected"
    }

    var products: List<ProductRealm> = emptyList()
        set(value) {
            field = value
            setChangedAndNotify(ProductModel.PRODUCTS)
        }

    var categories: List<CategoryRealm> = emptyList()
        set(value) {
            field = value
            setChangedAndNotify(ProductModel.CATEGORIES)
        }

    fun addCategory(cat: CategoryRealm){
        val newList = categories.filter { it.id!! <  cat.id!!}
        val mutable = newList.toMutableList()
        mutable.add(cat)
        categories = mutable.toList()
    }

    var productFeatures: List<ProductFeatureRealm> = emptyList()
        set(value) {
            field = value
            setChangedAndNotify(ProductModel.PRODUCT_FEATURES)
        }

    var productFeatureValuesSelected: MutableList<Namable> = mutableListOf()
        set(value) {
            field = value
            setChangedAndNotify(ProductModel.PRODUCT_FEATURE_VALUE_SELECTED)
        }

    private fun setChangedAndNotify(field: String) {
        setChanged()
        notifyObservers(field)
    }

}