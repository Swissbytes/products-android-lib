package ch.swissbytes.syscomappproducts.activity.products.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ch.swissbytes.syscomappbase.extensions.toBigDecimalOrZero
import ch.swissbytes.syscomappbase.extensions.toFormatDecimalDefault
import ch.swissbytes.syscomappproducts.BuildConfig
import ch.swissbytes.syscomappproducts.R
import ch.swissbytes.syscomappproducts.activity.products.ProductsActivityIntent
import ch.swissbytes.syscomappproducts.activity.products.fragments.adapter.ProductAdapter
import ch.swissbytes.syscomappproducts.activity.products.modals.ProductModal
import ch.swissbytes.syscomappproducts.activity.products.modals.StockBalance
import ch.swissbytes.syscomappproducts.daoandservices.getGlobalCurrencyService
import ch.swissbytes.syscomappproducts.entities.SaleProductParcelable
import java.math.BigDecimal
import java.util.*


/**
 * Called from [ch.swissbytes.syscomappproducts.fragments.ProductsTabFragment]
 * Responsible in showing the list of products already selected by the user
 *
 * @property onClose called when the fragment detach
 * @property trashClick listener used by the parent fragment the show the undo button
 * @property restoreDelayFinish listener used by the parent fragment the hide the undo button
 * @property beforeProductAdded optional, can prevent the product to be added (generally used to show a confirmation dialog)
 * @property beforeProductRemoved optional, can prevent the product to be removed (generally used to show a confirmation dialog)
 */
class ProductSelectedFragment : Fragment() {

    private val TAG = this::class.java.simpleName
    private var rrv: RecyclerView? = null
    private var emptyListView: View? = null
    private var totalPriceTv: TextView? = null

    var currentProductSelection: MutableList<SaleProductParcelable> = mutableListOf()
    private val currencyService by lazy { getGlobalCurrencyService() }
    private var originalProducts: List<SaleProductParcelable> = emptyList()
    private var canBeModified = false
    private var bId: Long? = null
    private var wId: Long? = null
    private var cId: String? = null
    private var isPriceListLegacyModeOn: Boolean = false
    private var defaultPriceListId: Long? = null
    private var constraintToStock: Boolean = false
    private var canOverflowAvailableStock: Boolean = false
    private var adapter: ProductAdapter? = null

    var onClose: ((List<SaleProductParcelable>) -> Unit)? = null
    var trashClick: ((SaleProductParcelable) -> Unit)? = null
    var restoreDelayFinish: ((Boolean) -> Unit)? = null
    var onCurrentSelectionChange: ((MutableList<SaleProductParcelable>) -> Unit)? = null
    var beforeProductAdded: ((position: Int, item: SaleProductParcelable, then: () -> Unit) -> Unit)? = null
    var beforeProductRemoved: ((position: Int, item: SaleProductParcelable,  then: () -> Unit) -> Unit)? = null

    private var productModal: ProductModal? = null

    fun restore(){
        adapter?.restore()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            currentProductSelection = it.getParcelableArrayList<SaleProductParcelable>(ARG_PRODUCTS_FRAGMENT)
                ?.toMutableList() ?: mutableListOf()

            bId = if (bId == null) it.getLong(ARG_B_ID) else bId
            wId = if (wId == null) it.getLong(ARG_W_ID) else wId
            cId = if (cId == null) it.getString(ARG_C_ID) else cId

            constraintToStock = it.getBoolean(ARG_CONSTRAINT_TO_STOCK)
            isPriceListLegacyModeOn = it.getBoolean(ARG_LEGACY_MODE)
            defaultPriceListId = it.getLong(ARG_DEFAULT_PRICE_LIST_ID)

            canOverflowAvailableStock = it.getBoolean(ARG_CAN_OVERFLOW_AVAILABLE_STOCK)
            canBeModified = it.getBoolean(ARG_CAN_BE_MODIFIED)

            originalProducts = currentProductSelection.toList()
        }
    }

    fun updateBusinessUnitId(businessUnitId: Long?) { bId = businessUnitId }

    fun updateWarehouseId(warehouseId: Long?) { wId = warehouseId }

    fun updateCustomerId(customerId: String?) { cId = customerId }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_product, container, false)
        rrv = v.findViewById(R.id.rrv); emptyListView = v.findViewById(R.id.emptyList); totalPriceTv = v.findViewById(R.id.totalPrice)

        adapter = ProductAdapter(currentProductSelection,
            canBeModified = canBeModified,
            onRestore = { onAdapterChange() },
            onCurrentSelectionChange = onCurrentSelectionChange,
            onProductClick = { i, p -> onProductSelectedClick(i, p) },
            onRestoreDelayFinish =  restoreDelayFinish
        ) {
            onAdapterChange()
            trashClick?.invoke(it)
        }

        rrv?.let { rv ->
            rv.adapter = adapter
            rv.addItemDecoration(DividerItemDecoration(context, LinearLayoutManager.VERTICAL))
        }

        onAdapterChange()
        return v
    }

    fun forceEmptyTrash() = adapter?.emptyTrash()

    /**
     * Called when the user click a product from the list of product selected
     * It's open the product dialog
     */
    private fun onProductSelectedClick(position: Int, item: SaleProductParcelable){
        if (productModal == null) {
            val productId = item.prodId!!
            val currentQtySelected = currentProductSelection.firstOrNull { p -> p.prodId == productId }?.qty ?: BigDecimal.ZERO
            productModal = ProductModal(
                    product = item,
                    currentQtySelected = currentQtySelected,
                    pIntent = ProductsActivityIntent(
                            businessUnitId = bId!!,
                            warehouseId = wId!!,
                            constraintToStock = constraintToStock,
                            stockBalances= stockBalance().toMutableList(),
                            canOverflowAvailableStock = canOverflowAvailableStock,
                            customerId = cId!!,
                            isPriceListLegacyModeOn = isPriceListLegacyModeOn,
                            priceListId = defaultPriceListId!!,
                            alreadySelectedProducts = mutableListOf(),
                            categoryId = 0L,
                            disableBackArrow = false
                    )
            )


            productModal?.show((activity as AppCompatActivity), { productModal = null }) { nItem ->
                val userAddQty = nItem.qty!! > item.qty!!
                val discountChange = nItem.discountPercentage != item.discountPercentage
                val unitMeasureChange = nItem.unitMeasureId != item.unitMeasureId
                val userRemoveQty = nItem.qty!! < item.qty!!

                if (BuildConfig.DEBUG) {
                    Log.i(TAG, "userAddQty: $userAddQty, discountChange: $discountChange, unitMeasureChange: $unitMeasureChange, userRemoveQty: $userRemoveQty")
                }

                if (userAddQty || discountChange || unitMeasureChange) {
                    beforeProductAdded?.invoke(position, item) { onProductGetUpdated(position, nItem) }
                }

                if (userRemoveQty) {
                    beforeProductRemoved?.invoke(position, item) { onProductGetUpdated(position, nItem) }
                }
            }
        }
    }

    fun stockBalance(): List<StockBalance> {
        return currentProductSelection.map { tmp ->
            val originalProduct = originalProducts.firstOrNull { p -> p.prodId == tmp.prodId }
            var plus = BigDecimal.ZERO
            var minus = BigDecimal.ZERO
            if (originalProduct != null){
                if (originalProduct.qty!! < tmp.qty!!){
                    //We remove from the truck stock the products already selected
                    minus = tmp.qty!! - originalProduct.qty!!
                } else if (originalProduct.qty!! > tmp.qty!!) {
                    //We add to the truck stock the products from the original order
                    plus = originalProduct.qty!! - tmp.qty!!
                }
            } else {
                minus = tmp.qty!!
            }
            StockBalance(productId = tmp.prodId!!, minus = minus, plus = plus)
        }
    }


    /**
     * Public API used when the list of product get changed outside of the library
     */
    fun updateProductList(p: MutableList<SaleProductParcelable>){
        adapter?.updateList(p)
        rrv?.adapter = adapter
        onAdapterChange()
    }

    private fun onProductGetUpdated(position: Int, item: SaleProductParcelable){
        adapter?.apply {
            items[position] = item
            notifyItemChanged(position, item)
            this@ProductSelectedFragment.onCurrentSelectionChange?.invoke(items)
        }

        calculateTotalPrice()
    }

    private fun onAdapterChange(){
        adapter?.apply {
            val listIsEmpty = itemCount == 0
            emptyListView?.apply {
                if (visibility == View.VISIBLE && !listIsEmpty)
                    visibility = View.GONE
                if (visibility == View.GONE && listIsEmpty)
                    visibility = View.VISIBLE
            }
        }

        calculateTotalPrice()
    }

    private fun calculateTotalPrice() {
        val totalAmountBob = currentProductSelection.fold(BigDecimal.ZERO) { a, b -> a.add(b.totalPrice.toBigDecimalOrZero()) }
        totalPriceTv?.text = context?.getString(R.string.format_total_amount,
            totalAmountBob.toFormatDecimalDefault(),
            currencyService.BOBtoUSD(totalAmountBob).toFormatDecimalDefault())
    }

    override fun onDetach() {
        super.onDetach()
        onClose?.invoke(adapter!!.items)
    }

    companion object {
        const val ARG_PRODUCTS_FRAGMENT = "ARG_PRODUCTS_FRAGMENT"
        const val ARG_B_ID = "ARG_B_ID"
        const val ARG_W_ID = "ARG_W_ID"
        const val ARG_C_ID = "ARG_C_ID"
        const val ARG_LEGACY_MODE = "ARG_LEGACY_MODE"
        const val ARG_DEFAULT_PRICE_LIST_ID = "ARG_DEFAULT_PRICE_LIST_ID"
        const val ARG_CONSTRAINT_TO_STOCK = "ARG_CONSTRAINT_TO_STOCK"
        const val ARG_CAN_BE_MODIFIED = "ARG_CAN_BE_MODIFIED"
        const val ARG_CAN_OVERFLOW_AVAILABLE_STOCK = "ARG_CAN_OVERFLOW_AVAILABLE_STOCK"
        @JvmStatic fun newInstance(
            list: ArrayList<SaleProductParcelable>,
            businessUnitId: Long,
            warehouseId: Long,
            customerId: String,
            isPriceListLegacyModeOn: Boolean,
            defaultPriceListId: Long,
            canBeModified: Boolean = false,
            constraintToStock: Boolean = false,
            canOverflowAvailableStock: Boolean = false
        ) = ProductSelectedFragment().apply {
            arguments = Bundle().apply {
                putParcelableArrayList(ARG_PRODUCTS_FRAGMENT, list)
                putLong(ARG_B_ID, businessUnitId)
                putLong(ARG_W_ID, warehouseId)
                putString(ARG_C_ID, customerId)
                putBoolean(ARG_LEGACY_MODE, isPriceListLegacyModeOn)
                putLong(ARG_DEFAULT_PRICE_LIST_ID, defaultPriceListId)
                putBoolean(ARG_CONSTRAINT_TO_STOCK, constraintToStock)
                putBoolean(ARG_CAN_BE_MODIFIED, canBeModified)
                putBoolean(ARG_CAN_OVERFLOW_AVAILABLE_STOCK, canOverflowAvailableStock)
            } }
    }
}
