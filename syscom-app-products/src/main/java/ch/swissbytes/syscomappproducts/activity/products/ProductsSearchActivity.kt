package ch.swissbytes.syscomappproducts.activity.products

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import ch.swissbytes.syscomappbase.extensions.showToast
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappproducts.BR
import ch.swissbytes.syscomappproducts.R
import ch.swissbytes.syscomappproducts.activity.products.modals.StockBalance
import ch.swissbytes.syscomappproducts.activity.products.vm.ProductSearchVM
import ch.swissbytes.syscomappproducts.activity.products.vm.model.ProductModel
import ch.swissbytes.syscomappproducts.daoandservices.ProductDao
import ch.swissbytes.syscomappproducts.entities.SaleProductParcelable
import kotlinx.android.synthetic.main.activity_products.view.*

data class ProductsActivityIntent(
        val alreadySelectedProducts: MutableList<SaleProductParcelable>,
        val stockBalances: MutableList<StockBalance>,
        val businessUnitId: Long,
        val customerId: String,
        val warehouseId: Long,
        val constraintToStock: Boolean,
        val categoryId: Long,
        val disableBackArrow: Boolean,
        val canOverflowAvailableStock: Boolean,
        val isPriceListLegacyModeOn: Boolean,
        val priceListId: Long
)


/**
 * A base class that provide a list of products to search
 * This class get a [ProductsActivityIntent] as it input and launch a [ProductSearchVM] a view
 */
abstract class ProductsActivity : AppCompatActivity() {

    companion object {
        const val INPUT = "products"
        const val STOCK_BALANCE = "stockBalances"
        const val B_ID = "businessUnitId"
        const val W_ID = "warehouseId"
        const val C_ID = "customerId"
        const val PRICE_LIST_LEGACY_MODE = "isPriceListLegacyModeOn"
        const val PRICE_LIST_ID = "priceListId"
        const val CONSTRAINT_TO_STOCK = "constraintToStock"
        const val CATEGORY_ID = "classificationId"
        const val ALLOW_BACK_ARROW = "disableBackArrow"
        const val CAN_OVERFLOW_AVAILABLE_STOCK = "canOverflowAvailableStock"

        const val DIRECT_SALE_QUIT = 2
    }

    /**
     * The product query filtering implementation
     */
    abstract fun getProductDaoImplementation(listener: GenericListener<ProductDao>)

    private val directSaleConfirmOptionMenuId = 101

    val pIntent: ProductsActivityIntent by lazy {
        ProductsActivityIntent(
            alreadySelectedProducts = intent.getParcelableArrayListExtra(INPUT) ?: mutableListOf(),
            stockBalances = intent.getParcelableArrayListExtra(STOCK_BALANCE) ?: mutableListOf(),
            businessUnitId = intent.getLongExtra(B_ID, -1),
            warehouseId = intent.getLongExtra(W_ID, -1),
            customerId = intent.getStringExtra(C_ID),
            constraintToStock = intent.getBooleanExtra(CONSTRAINT_TO_STOCK, false),
            categoryId = intent.getLongExtra(CATEGORY_ID, 1L),
            disableBackArrow = intent.getBooleanExtra(ALLOW_BACK_ARROW, false),
            canOverflowAvailableStock = intent.getBooleanExtra(CAN_OVERFLOW_AVAILABLE_STOCK, false),
            isPriceListLegacyModeOn = intent.getBooleanExtra(PRICE_LIST_LEGACY_MODE, false),
            priceListId = intent.getLongExtra(PRICE_LIST_ID, -1)
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        val binding = DataBindingUtil.setContentView<ViewDataBinding>(this, R.layout.activity_products)
        binding.setVariable(BR.vm, vm)
//        binding.root.rrv.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        setSupportActionBar(binding.root.toolbar)
        if (!pIntent.disableBackArrow) {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }

        getProductDaoImplementation {
            vm.productDao = it
        }
    }

    val vm:ProductSearchVM by lazy {
        ProductSearchVM(
                this,
                ProductModel(),
                pIntent = pIntent,
                onQuit = { onQuit() })
        { onResult(it) }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_products, menu)
        return true
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        if (menu.findItem(directSaleConfirmOptionMenuId) == null) {
            menu.add(Menu.NONE, directSaleConfirmOptionMenuId, 1, getString(R.string.confirm))
                .setShowAsActionFlags(MenuItem.SHOW_AS_ACTION_ALWAYS)
                .setIcon(R.drawable.ic_shopping_cart_white_24dp)
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onDestroy() {
        super.onDestroy()
        vm.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId){
            R.id.action_search -> {
                vm.search.openContextMenuToolbar()
                return true
            }
            android.R.id.home, directSaleConfirmOptionMenuId -> {
                val pSelected = vm.pIntent.alreadySelectedProducts.toList()
                if (pIntent.disableBackArrow && pSelected.isEmpty()){
                    showToast(getString(R.string.empty_cart_advice))
                } else {
                    onResult(pSelected)
                }
                return false
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        if (pIntent.disableBackArrow){
            onDirectSaleQuit()
        } else {
            onResult(vm.pIntent.alreadySelectedProducts.toList())
        }
    }

    private fun onQuit(){
        setResult(Activity.RESULT_CANCELED, Intent())
        finish()
    }

    private fun onDirectSaleQuit(){
        setResult(DIRECT_SALE_QUIT, Intent())
        finish()
    }

    private fun onResult(p: List<SaleProductParcelable>){
        val returnIntent = Intent()
        returnIntent.putParcelableArrayListExtra("result", ArrayList(p))
        setResult(Activity.RESULT_OK, returnIntent)
        finish()
    }

}
