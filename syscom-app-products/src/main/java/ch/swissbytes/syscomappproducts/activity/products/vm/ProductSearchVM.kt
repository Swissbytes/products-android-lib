package ch.swissbytes.syscomappproducts.activity.products.vm

import android.content.Context
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import ch.swissbytes.syscomappbase.extensions.postDelayed
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappproducts.BR
import ch.swissbytes.syscomappproducts.R
import ch.swissbytes.syscomappproducts.activity.products.ProductsActivityIntent
import ch.swissbytes.syscomappproducts.activity.products.modals.ProductModal
import ch.swissbytes.syscomappproducts.activity.products.modals.ProductModalVm
import ch.swissbytes.syscomappproducts.activity.products.modals.StockBalance
import ch.swissbytes.syscomappproducts.activity.products.vm.model.ProductModel
import ch.swissbytes.syscomappproducts.base.extensions.showSnackBar
import ch.swissbytes.syscomappproducts.base.extensions.toProductFeatureValueTwoDimensionalArray
import ch.swissbytes.syscomappproducts.dagger.DaggerWrapper
import ch.swissbytes.syscomappproducts.daoandservices.ClassificationDao
import ch.swissbytes.syscomappproducts.daoandservices.ProductDao
import ch.swissbytes.syscomappproducts.daoandservices.SearchToolBar
import ch.swissbytes.syscomappproducts.entities.*
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.list.listItemsMultiChoice
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject
import java.math.BigDecimal
import java.util.*
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * The ViewModel of the product search
 *
 * @param businessUnitId The businessId is needed by the [ProductModalVm] to retrieve the product price
 * @param warehouseId The warehouseId is needed by the [ProductModalVm] to retrieve the stock available getStockAvailableByWarehouseId
 * @param productSelected The list of products already selected by the user
 *
 * @property search the [ch.swissbytes.syscomappproducts.daoandservices.SearchToolBar] class responsible to handle the top toolbar edittext search
 *
 * @property products The products displayed
 * @property productFeatures (ej: TAMANO, COLOR, TIPO, PESO) the list of feature displayed in the recyclerview below the list of categories. The OnClickListener
 * is defined here [getProductFeatureAdapterListener]
 *
 * @property allProductFeatureValues (ej: (large, small), (blue, green)) the list of featureValue displayed in the dropdown menu
 * is defined here [getProductFeatureAdapterListener]
 *
 * @property productFeatureValuesSelected the list of tags displaying the features selected by the user
 * the list adapter is configured here [ch.swissbytes.syscomappproducts.binders.productFeatureAdapter] and the click listener
 * here [getProductFeatureValuesSelectedAdapterListener]. On click on a tag the tak is removed and the list of product updated
 */

class ProductSearchVM(
        val activity: AppCompatActivity,
        private val productM: ProductModel,
        val pIntent: ProductsActivityIntent,   // Required to set up the price
        val onQuit: () -> Unit,
        val onResult: (List<SaleProductParcelable>) -> Unit
) : Observer, BaseObservable() {

    val TAG = this.javaClass.simpleName

    @Inject lateinit var classificationDao: ClassificationDao
    @Inject lateinit var search: SearchToolBar

    private var disposableForSearch: Disposable? = null
    private var disposableForCriteria: Disposable? = null

    var allProductFeatureValues: List<ProductFeatureValueRealm> = emptyList()
    private var textSearchString = BehaviorSubject.create<String>()

    var productDao: ProductDao? = null
    set(value) {
        field = value
        updateSearchCriterias()
    }

    var isSearching: Boolean = false
    set(value) {
        field = value
        notifyPropertyChanged(BR.showLoader)
        notifyPropertyChanged(BR.showEmptyListMessage)
    }

    private var classificationId: Long = 0
    /**
     * Add activity dependency to the [ch.swissbytes.syscomappproducts.daoandservices.SearchToolBar] toolbar class
     * Setup the model observer
     */
    init {
        if (pIntent.businessUnitId == -1L){ throw RuntimeException("A businessUnitId is required to run this activity") }
        classificationId = pIntent.categoryId
        DaggerWrapper.getComponent(activity)!!.inject(this)
        productM.addObserver(this)
        search.activity = activity
        setUpSearchInputLogic()

        classificationDao.getById(pIntent.categoryId)
            ?.let { productM.addCategory(it) }

        isSearching = true //Wait for the productDao field to be set
    }

    /**
     * The logic to filter and restore the [products] displayed after the user search from the search InputField
     */
    private fun setUpSearchInputLogic(){
        search.onClose = {
            searchText = ""
            products = productsBeforeEditTextInputFiltering
        }
        search.onOpen = {
            productsBeforeEditTextInputFiltering = products
        }

        if (disposableForSearch != null) disposableForSearch?.dispose()

        disposableForSearch = textSearchString.debounce(500, TimeUnit.MILLISECONDS)
            .subscribe { value -> onUserInputSearchTextChange(value) }
    }

    private fun onUserInputSearchTextChange(value: String){
        products = if (value.isBlank())
            productsBeforeEditTextInputFiltering else
            productsBeforeEditTextInputFiltering.filter{ it.name?.contains(value, ignoreCase = true) == true || it.code?.contains(value, ignoreCase = true) == true }
    }

    /**
     * When a product is added to the [productSelected] from the [ProductModal] dialog
     */
    private fun addProduct(prod: SaleProductParcelable){
        Log.d(TAG, "prodToAdd: ${prod.string()}")
        Log.d(TAG, "allProductSelected before: ${pIntent.alreadySelectedProducts}")

        /**
         * If the product was already selected just update it's qty
         * else add it to the current selection [productSelected]
         */
        val originalProd = pIntent.alreadySelectedProducts.firstOrNull { it.prodId == prod.prodId }
        if (originalProd != null){
            originalProd.qty = originalProd.qty!! + prod.qty!!
            val stockBalance = pIntent.stockBalances.firstOrNull { it.productId == prod.prodId }
            //if the user already add some qty from the stock to this product we have to substract it from the stock
            stockBalance?.minus?.plus(prod.qty!!)?.let { stockBalance.minus = it }
        } else {
            pIntent.stockBalances.add(StockBalance(prod.prodId!!, prod.qty!!))
            pIntent.alreadySelectedProducts.add(prod)
        }

        Log.d(TAG, "allProductSelected after: ${pIntent.alreadySelectedProducts}")
        notifyPropertyChanged(BR.selectedProductCount)
        activity.showSnackBar(R.string.prod_added)
    }

    /**
     * Update the products displayed depending of the [classificationId] and [productFeatureValuesSelected] selected
     * by calling [BaseProductDao.getSearchRequestAsync]
     */
    private fun updateSearchCriterias(){
        val listOfProductFeatureValueSelectedIds = productFeatureValuesSelected.map { it.id!! }.toList()
        isSearching = true

        if (disposableForCriteria != null) disposableForCriteria?.dispose()

        disposableForCriteria = productDao?.getSearchRequestAsync(
                    categoryId = pIntent.categoryId,
                    featureValuesSelected = listOfProductFeatureValueSelectedIds.toProductFeatureValueTwoDimensionalArray()
            )?.subscribe {
                    productM.products =
                        if (!pIntent.constraintToStock) it.products
                        else it.products.filter { it.getStockAvailableByWarehouseId(pIntent.warehouseId) != null }

                    productsBeforeEditTextInputFiltering = productM.products

                    productM.productFeatures = it.productFeature
                    allProductFeatureValues = it.productFeatureValue

                    if (productFeatureValuesSelected.isEmpty() || productFeatures.isEmpty()){
                        postDelayed(100L) {
                            activity.runOnUiThread {
                                requestLayout()
                                Log.w(TAG, "requestLayout called")
                            }
                        }
                    }

                    isSearching = false
            }
    }

    private fun requestLayout(){
        notifyPropertyChanged(BR.productFeatures)
        notifyPropertyChanged(BR.productFeatureValuesSelected)
    }

    fun onDestroy(){
        disposableForSearch?.dispose()
        disposableForCriteria?.dispose()
    }

    override fun update(o: Observable?, fieldChanged: Any?) {
        if (fieldChanged is String){
            when (fieldChanged){
                ProductModel.PRODUCTS -> {
                    notifyPropertyChanged(BR.products)
                    notifyPropertyChanged(BR.productSize)
                }
                ProductModel.PRODUCT_FEATURES -> notifyPropertyChanged(BR.productFeatures)
                ProductModel.PRODUCT_FEATURE_VALUE_SELECTED -> notifyPropertyChanged(BR.productFeatureValuesSelected)
                ProductModel.CATEGORIES -> notifyPropertyChanged(BR.categories)
            }
        }
    }

    private var productsBeforeEditTextInputFiltering: List<ProductRealm> = emptyList()

    var searchText: String = "" @Bindable
        set(value) {
            field = value
            textSearchString.onNext(value)
        }

    var productSize: String = "" @Bindable get() = products.size.toString()

    var showLoader: Boolean = false @Bindable get() = products.isEmpty() && isSearching

    var showEmptyListMessage: Boolean = false @Bindable get() = products.isEmpty() && !isSearching

    var products: List<ProductRealm> = emptyList() @Bindable get() = productM.products
        set(value) {
            field = value
            productM.products = field
        }

    var selectedProductCount: String = "0" @Bindable get() = pIntent.alreadySelectedProducts.size.toString()

    var productFeatures: List<ProductFeatureRealm> = emptyList() @Bindable get() = productM.productFeatures
        set(value) {
            field = value
            productM.productFeatures = field
        }

    var productFeatureValuesSelected: MutableList<Namable> = mutableListOf() @Bindable get() = productM.productFeatureValuesSelected
        set(value) {
            field = value
            productM.productFeatureValuesSelected = field
        }

    var categories: List<CategoryRealm> = emptyList() @Bindable get() = productM.categories
        set(value) {
            field = value
            productM.categories = field
        }

    var undo: Boolean = false @Bindable get() = field
        set(value) {
            field = value
            notifyPropertyChanged(BR.undo)
        }

    private var pModal: ProductModal? = null

    /**
     * On [products] product click
     */
    @Bindable
    fun getProductAdapterListener(): (product: Namable) -> Unit = {
        var prodClicked = getProductSelectedById(it)

        pModal = ProductModal(
            product = prodClicked,
            pIntent = pIntent
        )
        pModal?.show(activity = activity, onDismiss = {
            pModal = null
            search.closeContextMenuToolbar()
        }) { p -> addProduct(p) }
    }

    /**
     * If not present in the list of selected product we create a new [SaleProductParcelable]
     */
    private fun getProductSelectedById(prod: Namable)
            = pIntent.alreadySelectedProducts.firstOrNull { it.prodId == prod.id }
        ?: SaleProductConcrete().apply {
            prodId = prod.id
            name = prod.name
            qty = BigDecimal.ZERO
            discountPercentage = ""
        }


    /**
     * Remove the featureValues (tag) selected and update the product list
     */
    @Bindable
    fun getProductFeatureValuesSelectedAdapterListener(): GenericListener<Namable> = {
        productM.productFeatureValuesSelected.remove(it)
        notifyPropertyChanged(BR.productFeatureValuesSelected)
        updateSearchCriterias()
    }

    /**
     * When a feature is clicked (ej: COLOR) show a multiselect dialog with the available options (ej: RED, BLUE, GREEN)
     * and call [onProductFeatureValueAdded] to update the [productFeatureValuesSelected] which is binded to the display
     */
    @Bindable
    fun getProductFeatureAdapterListener(): GenericListener<Namable> = { feature ->
        val featureValuesList = allProductFeatureValues.filter { it.productFeature!!.id == feature.id }
        val featureValuesNames = featureValuesList.map { it.name!! }
        showProductFeaturePickDialog(featureValuesList, featureValuesNames)
    }

    private fun showProductFeaturePickDialog(featureValuesList: List<ProductFeatureValueRealm>, featureValuesNames: List<String>){
        MaterialDialog(activity).show {
            listItemsMultiChoice(items = featureValuesNames)
            { _, indexes, _ ->
                val list = featureValuesList.filterIndexed { index, _ -> indexes.contains(index)}
                onProductFeatureValueAdded(list)
            }
            positiveButton(android.R.string.ok)
        }
    }

    private fun onProductFeatureValueAdded(featureValues: List<Namable>){
        val newSelection = featureValues.filter { fv -> !productM.productFeatureValuesSelected.any { fv.id == it.id } }
        productM.productFeatureValuesSelected.addAll(newSelection)
        notifyPropertyChanged(BR.productFeatureValuesSelected)
        updateSearchCriterias()
    }

    /**
     * Show a dialog containing all the possible child classification from the classification selected
     * if [getClassificationChildOfAsync] return a emptyList it's men a leaf is reach
     * we call [updateCurrentClassificationSelected] when the user click on a childClassification
     * which add a category to the rrv and update the list of products [updateSearchCriterias]
     */
    @Bindable
    fun getClassificationClick(): (feature: Namable) -> Unit = { classification ->
        classificationDao.getClassificationChildOfAsync(classification.id!!).subscribe {
            val list = it
            if (list.isNotEmpty()){
                val categoriesName = list.map { it.name!! }
                MaterialDialog(activity as Context).show {
                    listItemsSingleChoice(items = categoriesName) { _, index, _ -> updateCurrentClassificationSelected(list.get(index)) }
                }
            }
        }
    }

    @Bindable
    fun getClassificationBackNavigation(): (feature: Namable) -> Unit = {
        updateCurrentClassificationSelected(classificationDao.getById(it.id!!)!!)
    }

    @Synchronized private fun updateCurrentClassificationSelected(cat: CategoryRealm){
        classificationId = cat.id!!
        productM.addCategory(cat)
        updateSearchCriterias()
    }

}