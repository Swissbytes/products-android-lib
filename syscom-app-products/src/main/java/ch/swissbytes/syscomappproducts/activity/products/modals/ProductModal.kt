package ch.swissbytes.syscomappproducts.activity.products.modals

import android.content.Context
import android.os.Parcelable
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import ch.swissbytes.syscomappbase.extensions.*
import ch.swissbytes.syscomappbase.listeners.SimpleListener
import ch.swissbytes.syscomappproducts.BR
import ch.swissbytes.syscomappproducts.BuildConfig
import ch.swissbytes.syscomappproducts.R
import ch.swissbytes.syscomappproducts.activity.products.ProductsActivityIntent
import ch.swissbytes.syscomappproducts.base.extensions.realmGetItemCopy
import ch.swissbytes.syscomappproducts.base.extensions.subPercent
import ch.swissbytes.syscomappproducts.base.extensions.toLongOrZero
import ch.swissbytes.syscomappproducts.daoandservices.PriceDao
import ch.swissbytes.syscomappproducts.databinding.ModalProductBinding
import ch.swissbytes.syscomappproducts.entities.*
import ch.swissbytes.syscomappproducts.realm.RealmProduct
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.callbacks.onDismiss
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import kotlinx.android.parcel.Parcelize
import java.math.BigDecimal


/**
 * Called from the screen of products in stock: [ch.swissbytes.syscomappproducts.activity.products.vm.ProductSearchVM]
 * and by the sreen of products selected [ch.swissbytes.syscomappproducts.activity.products.fragments.ProductSelectedFragment]
 *
 */
class ProductModal(
    private val product: SaleProductParcelable,
    private val pIntent: ProductsActivityIntent,
    private val currentQtySelected: BigDecimal? = null
) {

    private val TAG = this::class.java.simpleName
    private var onConfirm: ((SaleProductParcelable) -> Unit)? = null

    fun show(
        activity: AppCompatActivity,
        onDismiss: (() -> Unit)? = null,
        _onConfirm: ((SaleProductParcelable) -> Unit)? = null
    ) {
        onConfirm = _onConfirm
        val dialog = MaterialDialog(activity)
            .customView(R.layout.modal_product, scrollable = true)
            .title(text = product.name)
        val customView = dialog.getCustomView()
        val mBinding = ModalProductBinding.bind(customView)
        var dialogShouldOpen = true

        val vm = ProductModalVm(
                activity,
                productInterface = product,
                pIntent = pIntent,
                currentQtySelected = currentQtySelected ?: BigDecimal.ZERO
        ) {
            activity.showToast(R.string.no_stock_available)
            dialogShouldOpen = false
            if (dialog.isShowing){
                dialog.dismiss()
            }
        }

        if (dialogShouldOpen){
            mBinding.vm = vm
            customView.apply {
                dialog.onDismiss { onDismiss?.invoke() }
                dialog.positiveButton { onProductConfirm(vm.getSaleProduct()) }
                dialog.show()
            }
        }
    }

    private fun onProductConfirm(saleP: SaleProductParcelable) {
        val valid: Boolean = (saleP.qty != null && saleP.qty!!.greaterThanZero())
        if (valid) {
            Log.i(TAG, "onProductConfirm: ${saleP.string()}")
            onConfirm?.invoke(saleP)
        }
    }

}


/**
 *  @param minus if the user already add some qty from the stock to this product
 *              we have to substract it from the available stock
 *
 *
 *  @param plus if the user had removed some qty from the original order
 *              we have to add it to the stock as to fake that they are available for sale
 *
 * */
@Parcelize
data class StockBalance(val productId: Long, var minus: BigDecimal = BigDecimal.ZERO, var plus: BigDecimal = BigDecimal.ZERO): Parcelable

/**
 * @param productId The product to dosplay
 * @param bId the businessId the price should be taken from
 * @param wId the warehouseId the stock should be taken from
 */
class ProductModalVm(
    val context: Context,
    private val productInterface: SaleProductParcelable,
    private val pIntent: ProductsActivityIntent,
    private val currentQtySelected: BigDecimal,
    private val onNoStockAvailable: SimpleListener? = null
) : BaseObservable() {

    private val productId by lazy { productInterface.prodId!! }
    private val priceDao by lazy {  PriceDao(pIntent.isPriceListLegacyModeOn, pIntent.priceListId) }
    /**
     * The product clicked be the user
     */
    private val product: ProductRealm by lazy {
        realmGetItemCopy(
            ProductRealm::class.java,
            realm = RealmProduct.getDefaultInstance()) { equalTo(ProductRealm.ID, productId) }!!
    }

    /**
     * The stock for this product
     */
    private val pStock: ProductStockRealm? by lazy { product.getStockAvailableByWarehouseId(pIntent.warehouseId) }

    /**
     * The stock enable in the dialog box
     * @see [StockBalance]
     */
    private val stockBigDecimal: BigDecimal by lazy {
       val stockAvailable = if (currentQtySelected.greaterThanZero()) // When the user call the modal from outside the lib
           (pStock?.qtyAsBigDecimal ?:  BigDecimal.ZERO) + currentQtySelected - stockBalance.minus
       else
           (pStock?.qtyAsBigDecimal ?: BigDecimal.ZERO) + stockBalance.plus - stockBalance.minus

        if (stockAvailable.isZero() && !pIntent.canOverflowAvailableStock) onNoStockAvailable?.invoke()

        stockAvailable
    }

    private val stockBalance by lazy {
        pIntent.stockBalances.firstOrNull { it.productId == productId } ?: StockBalance(productId, BigDecimal.ZERO, BigDecimal.ZERO)
    }

    private val price: BigDecimal? by lazy {
        priceDao.getProductPriceWithCustomerIdString(productId, pIntent.businessUnitId, pIntent.customerId) }

    private val prodUnitMeasures: List<ProductUnitMeasureRealm> by lazy { product.unitMeasures.filter { it.name != null } }

    var qty = if (currentQtySelected.greaterThanZero()) currentQtySelected.stripTrailingZeros().toString() else ""
        @Bindable get() = field
        set(value) {
            val safeValue = if (value.isNumber()) value else BigDecimal.ZERO.toString()
            field = safeValue
            updateTotalPrices()
            notifyPropertyChanged(BR.qtyIsValid)
        }

    var discountPercentage = productInterface.discountPercentage
        @Bindable get() = field
        set(value) {
            val safeValue = if (value.isNumber()) value else BigDecimal.ZERO.toString()
            field = safeValue
            updateTotalPrices()
        }

    var qtyIsValid: Boolean = false @Bindable get() = qty.isNumber() && qty.toBigDecimalOrZero().greaterThanZero()

    var unitMeasures: List<String> = emptyList() @Bindable get() = prodUnitMeasures.map { it.name!!.toUpperCase() }

    var stock = ""
        @Bindable get() = if (BuildConfig.DEBUG) stockBigDecimal.toString()
        else if (stockBigDecimal .greaterOrEqualTo(BigDecimal.ZERO)) stockBigDecimal.toString() else "0"
        set(value) {
            field = value;
            notifyPropertyChanged(BR.stock)
            notifyPropertyChanged(BR.stockAsBigDecimal)
        }

    var constraintStock = pIntent.constraintToStock @Bindable get() = pIntent.constraintToStock

    var stockAsBigDecimal = stockBigDecimal @Bindable get() = stockBigDecimal

    var unitPrice = ""
        @Bindable get() = price.toFormattedString()
        set(value) {
            field = value; notifyPropertyChanged(BR.unitPrice)
        }

    var unitMeasurePosition = 0
        @Bindable get() = field
        set(value) {
            field = value;
            notifyPropertyChanged(BR.unitMeasurePosition)
            Log.i("TAG", "unitMeasurePosition $field")
        }

    var totalPrice = ""
        @Bindable get() = field
        set(value) {
            field = value; notifyPropertyChanged(BR.totalPrice)
        }

    var totalPriceWithCustomDiscount = ""
        @Bindable get() = field
        set(value) {
            field = value; notifyPropertyChanged(BR.totalPriceWithCustomDiscount)
        }

    private fun updateTotalPrices() {
        if (qty.isNumber() && qty.toBigDecimalOrZero().greaterThanZero()){
            val total = qty.toBigDecimalOrZero().multiplyBy(price!!)
            totalPrice = if (total.greaterThanZero()) total.toFormattedString() else ""
            totalPriceWithCustomDiscount =
                if (customDiscountIsSet()) total.subPercent(discountPercentage.toBigDecimalOrZero()).toFormattedString() else ""
        }
    }

    private fun customDiscountIsSet() = discountPercentage.isNumber() && discountPercentage.toBigDecimalOrZero().greaterThan(BigDecimal.ZERO)

    fun getSaleProduct(): SaleProductParcelable {
        val unitPrice = unitPrice.replace(",", "")
        val totalPrice = (if (customDiscountIsSet()) totalPriceWithCustomDiscount else totalPrice).replace(",", "")
        return SaleProductConcrete(
            prodId = productId,
            qty = qty.toBigDecimalOrNull(),
            unitPrice = unitPrice, // replace(",", "")
            discountPercentage = discountPercentage,
            totalPrice = totalPrice.toBigDecimalOrZero().toString(), // with discount
            name = product.name,
            trackingType = product.trackingType,
            mustControlledOnSale = product.mustControlledOnSale,
            hasWarranty = product.hasWarranty,
            unitMeasureId = if (prodUnitMeasures.isNotEmpty()) prodUnitMeasures[unitMeasurePosition].id else null
        )
    }

}