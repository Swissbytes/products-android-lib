package ch.swissbytes.syscomappproducts.activity.products.fragments.adapter

import android.content.Context
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ch.swissbytes.syscomappbase.extensions.greaterThanZero
import ch.swissbytes.syscomappbase.extensions.setHtmlText
import ch.swissbytes.syscomappbase.extensions.toBigDecimalOrZero
import ch.swissbytes.syscomappbase.extensions.toFormattedString
import ch.swissbytes.syscomappproducts.R
import ch.swissbytes.syscomappproducts.daoandservices.getGlobalCurrencyService
import ch.swissbytes.syscomappproducts.entities.SaleProductParcelable
import java.math.BigDecimal

data class ProductInTrash(val position: Int, val p: SaleProductParcelable)

/**
 * called from [ch.swissbytes.syscomappproducts.activity.products.fragments.ProductSelectedFragment]
 * The adapter showing the list of product already selected by the user
 */
class ProductAdapter(
    val items: MutableList<SaleProductParcelable>,
    val canBeModified: Boolean = false,
    val onRestore: (() -> Unit)? = null,
    val onRestoreDelayFinish: ((Boolean) -> Unit)? = null,
    val onProductClick: ((position: Int, item: SaleProductParcelable) -> Unit)? = null,
    val onCurrentSelectionChange: ((MutableList<SaleProductParcelable>) -> Unit)? = null,
    private val conditionBeforeProductChange: ((SaleProductParcelable, BigDecimal) -> Boolean)? = null,
    private val trashClick: ((SaleProductParcelable) -> Unit)? = null
): RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    var context: Context? = null

    val currency by lazy { getGlobalCurrencyService() }
    private var productInTrash: MutableList<ProductInTrash> = mutableListOf()
    private val handler = Handler()
    private val runnable: Runnable = object: Runnable{ override fun run() { emptyTrash() } }

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        context = parent.context
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.row_product, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.apply {
            tv1.text = item.name
            tv2.text = context?.getString(R.string.format_prod_qty_small, item.qty.toString())


//          /*BONOS*/|| componentRule.id == -1L /*RECARGO*/


            val addedByPricingLibrary = item.addedByPricingLibType ?: 1 < 1

            if (!addedByPricingLibrary){
                val hasDiscount = item.discountPercentage.toBigDecimalOrZero().greaterThanZero()

                val totalPrice = if (hasDiscount)
                    context!!.getString(R.string.format_total_and_discount_bob_usd, item.discountPercentage,
                            item.totalPrice.toFormattedString(),
                            currency.BOBtoUSD(item.totalPrice?.toBigDecimal()).toFormattedString())
                else
                    context!!.getString(R.string.format_bob_usd, item.totalPrice.toFormattedString(),
                            currency.BOBtoUSD(item.totalPrice?.toBigDecimal()).toFormattedString())

                tv3.setHtmlText(totalPrice)
            } else {
                tv3.setHtmlText(context!!.getString(if (item.addedByPricingLibType == 1L) R.string.free_surcharge else R.string.free_bonus))
            }

            if (canBeModified && !addedByPricingLibrary){
                itemView.setOnClickListener { onProductClick?.invoke(position, item) }
                img.visibility = View.VISIBLE
                imgContainer.visibility = View.VISIBLE
                listOf(img, imgContainer).forEach { it.setOnClickListener{ beforeTrashClick(position, item, item.qty!!) } }
            }
        }
    }

    private fun beforeTrashClick(position: Int, item: SaleProductParcelable, qty: BigDecimal){
        if (conditionBeforeProductChange != null){
            if (conditionBeforeProductChange.invoke(item, qty)){
                putProductOnTmpTrash(position, item)
            }
        } else {
            putProductOnTmpTrash(position, item)
        }
    }

    fun updateList(p: MutableList<SaleProductParcelable>){
        items.clear()
        items.addAll(p)
        notifyDataSetChanged()
    }

    @Synchronized fun emptyTrash(){
        if (productInTrash.isNotEmpty()){
            handler.removeCallbacks(runnable)
            productInTrash.clear()
            onRestoreDelayFinish?.invoke(items.isEmpty())
            notifyDataSetChanged()
            onCurrentSelectionChange?.invoke(items)
        }
    }

    private fun putProductOnTmpTrash(_position: Int, item: SaleProductParcelable) {
        val position = items.indexOf(item)
        productInTrash.add(ProductInTrash(_position, item))
        removeItemAt(position)
        trashClick?.invoke(item)
        handler.removeCallbacks(runnable)
        handler.postDelayed(runnable, 3000)
    }

    private fun removeItemAt(position: Int){
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    @Synchronized fun restore(){
        productInTrash.sortBy { it.position }
        productInTrash.forEach {
            items.add(it.position, it.p)
            notifyItemInserted(it.position)
        }
        productInTrash.clear()
        onRestore?.invoke()
        onCurrentSelectionChange?.invoke(items)
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val tv1 = view.findViewById<TextView>(R.id.text1)
        val tv2 = view.findViewById<TextView>(R.id.text2)
        val tv3 = view.findViewById<TextView>(R.id.text3)
        val img = view.findViewById<View>(R.id.img)
        val imgContainer = view.findViewById<View>(R.id.img_container)
    }
}