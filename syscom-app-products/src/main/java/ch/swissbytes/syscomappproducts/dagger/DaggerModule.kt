package ch.swissbytes.syscomappproducts.dagger

import android.content.Context
import ch.swissbytes.syscomappproducts.daoandservices.ClassificationDao
import ch.swissbytes.syscomappproducts.daoandservices.SearchToolBar
import ch.swissbytes.syscomappproducts.prefs.AppPref
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


class DaggerWrapper(val context: Context) {
    companion object {
        private var mComponent: Component? = null

        fun getComponent(context: Context): Component? = mComponent ?: initComponent(context)

        private fun initComponent(context: Context): Component = DaggerComponent.builder().daggerModule(DaggerModule(context)).build()
    }
}

@Module
class DaggerModule(private val context: Context) {

    @Provides @Singleton
    fun provideApplication(): Context = context

    @Provides @Singleton
    fun pref(context: Context): AppPref = AppPref(context)

    @Provides @Singleton
    fun classificationDao() = ClassificationDao()

    @Provides @Singleton
    fun searchToolBar() = SearchToolBar()

}
