package ch.swissbytes.syscomappproducts.dagger

import ch.swissbytes.syscomappproducts.activity.products.vm.ProductSearchVM
import ch.swissbytes.syscomappproducts.services.ProductSync
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(DaggerModule::class))
interface Component {
    fun inject(productVM: ProductSearchVM)
    fun inject(pService: ProductSync)
}