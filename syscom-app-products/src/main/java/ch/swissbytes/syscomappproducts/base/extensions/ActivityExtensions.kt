package ch.swissbytes.syscomappproducts.base.extensions

import android.util.Log
import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.snackbar.Snackbar
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import com.google.gson.JsonSyntaxException

fun AppCompatActivity.showSnackBar(@StringRes message: Int){
    val parentLayout = findViewById<View>(android.R.id.content)
    Snackbar.make(parentLayout, message, Snackbar.LENGTH_LONG).show()
}

fun String.toPrettyJson():String {
    if (startsWith("{") || startsWith("[")) {
        try { return GsonBuilder().setPrettyPrinting().create().toJson(JsonParser().parse(this)) } catch (m: JsonSyntaxException) { Log.d("JSON", this) }
    }
    return "Can't format string $this"
}