package ch.swissbytes.syscomappproducts.base.extensions

import ch.swissbytes.syscomappproducts.entities.ProductFeatureValueRealm
import ch.swissbytes.syscomappproducts.realm.RealmProductConf
import io.realm.Realm

/**
 * Group productFeatureValues by productFeature where
 * Ex: [1, 2, 3, 4, 5, 6, 7] return [[1,3,7], [2, 4, 5, 6]] if productFeatureValues [1,3,7] and productFeatureValues [2, 4, 5, 6]
 * are from the same productFeature
 *
 * @return A list of list of productFeatureValues
 */
fun List<Long>.toProductFeatureValueTwoDimensionalArray() : List<List<Long>> {
    Realm.getInstance(RealmProductConf.config).use {
        val productFeatureValues = it.where(ProductFeatureValueRealm::class.java).`in`(ProductFeatureValueRealm.ID, this.toTypedArray()).findAll()
        var lastId = -1L
        val list = mutableListOf<List<Long>>()
        var curList = mutableListOf<Long>()
        productFeatureValues.forEach {
            val curId = it.productFeature?.id!!
            if (curId != lastId) {
                lastId = curId
                if (!curList.isEmpty()) {
                    list.add(curList.toList())
                }
                curList = mutableListOf()
            }
            curList.add(it.id!!)
        }
        if (curList.isNotEmpty()) {
            list.add(curList.toList())
        }
        return list.toList()
    }
}




