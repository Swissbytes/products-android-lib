package ch.swissbytes.syscomappproducts.base.extensions

import java.math.BigDecimal

fun BigDecimal.subPercent(pct: BigDecimal): BigDecimal {
    val percent =  this.multiply(pct).divide(BigDecimal(100L), 2, BigDecimal.ROUND_HALF_UP)
    return this.subtract(percent)
}

fun String?.toLongOrZero(): Long = if (this == null) 0L else this.toLongOrNull() ?: 0L