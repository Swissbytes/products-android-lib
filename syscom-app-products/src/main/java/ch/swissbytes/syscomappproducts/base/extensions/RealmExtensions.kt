package ch.swissbytes.syscomappproducts.base.extensions

import io.realm.*


fun realmTransaction(realm: Realm = Realm.getDefaultInstance(), f: (Realm) -> Unit)
        = realmUse(realm) { executeTransaction { r -> f(r) } }

fun realmPersist(model: Any, realm: Realm = Realm.getDefaultInstance())
        = realmUse(realm) {
                when(model){
                    is RealmModel -> realmTransaction(realm) { r -> r.copyToRealmOrUpdate(model) }
                    is List<*> -> realmTransaction(realm) { r -> r.copyToRealmOrUpdate(model.checkRealmModelList()!!) }
                } }

fun realmUse(realm: Realm = Realm.getDefaultInstance(), f: Realm.() -> Unit) = realm.use { f(it) }

fun <T: RealmModel> realmGetListCopy(myClass: Class<T>, realm: Realm = Realm.getDefaultInstance(), f: (RealmQuery<T>) -> List<T>): List<T> =
        realm.use {
            val response = f(it.where(myClass))
            if (response.isEmpty()) emptyList() else it.copyFromRealm(response)
        }

fun <T: RealmModel> realmGetOrderedList(myClass: Class<T>, realm: Realm = Realm.getDefaultInstance(), f: (RealmQuery<T>) -> RealmQuery<T>):
        Pair<Realm, OrderedRealmCollection<T>> = Pair(realm, f(realm.where(myClass)).findAll())

fun <T: RealmModel> realmGetItem(myClass: Class<T>, realm: Realm = Realm.getDefaultInstance(), f: (RealmQuery<T?>) -> RealmQuery<T?>):
        Pair<Realm, T?> = Pair(realm, f(realm.where(myClass)).findFirst())

fun <T: RealmModel> realmGetItemCopy(myClass: Class<T>, realm: Realm = Realm.getDefaultInstance(), f: RealmQuery<T?>.() -> RealmQuery<T?>):
       T? = realm.use {
            val item = f(it.where(myClass)).findFirst()
            if (item != null) it.copyFromRealm(item) else null
        }

fun <T: RealmModel> realmGetList(myClass: Class<T>, realm: Realm = Realm.getDefaultInstance(), f: ((RealmQuery<T?>) -> RealmQuery<T?>)? = null):
        RealmResults<T?> = realm.use {
        return if (f == null) it.where(myClass).findAll() else f(it.where(myClass)).findAll()
}

fun <T: RealmModel>  Realm.nextId(clazz: Class<T>): Long {
    val currentIdNum = this.where(clazz).max("id")
    return if (currentIdNum == null) 1L else currentIdNum.toLong() + 1L
}


@Suppress("UNCHECKED_CAST")
fun List<*>.checkRealmModelList() = if (all { it is RealmModel }) this as List<RealmModel> else null

