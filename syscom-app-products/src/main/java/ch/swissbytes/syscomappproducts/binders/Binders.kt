package ch.swissbytes.syscomappproducts.binders

import android.widget.ArrayAdapter
import android.widget.EditText
import android.widget.Spinner
import androidx.databinding.BindingAdapter
import ch.swissbytes.syscomappbase.utils.NumberConstraintTextWatcher
import ch.swissbytes.syscomappproducts.R
import java.math.BigDecimal

@BindingAdapter("constraintMax", "maxValue")
fun numberConstraint(tv: EditText, constraintMax: Boolean, maxValue: BigDecimal) {
    if (constraintMax){
        tv.addTextChangedListener(NumberConstraintTextWatcher(minNum = BigDecimal.ZERO, maxNum = maxValue))
    }
}

@BindingAdapter("items", "defaultMessageIfEmpty", requireAll = false)
fun spinner(sp: Spinner, list: List<String>, message: String?) {
    //TODO layout has been added to base
    sp.adapter = ArrayAdapter(sp.context, R.layout.spinner_adapter_item, list)

    if(message != null){
        sp.prompt = if (sp.adapter.count == 0) message else null
    }
}