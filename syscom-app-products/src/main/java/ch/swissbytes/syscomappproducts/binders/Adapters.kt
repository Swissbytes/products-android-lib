package ch.swissbytes.syscomappproducts.binders

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.annotation.LayoutRes
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import ch.swissbytes.syscomappbase.extensions.setHtmlText
import ch.swissbytes.syscomappbase.extensions.toFormattedString
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappbase.utils.NumberConstraintTextWatcher
import ch.swissbytes.syscomappproducts.R
import ch.swissbytes.syscomappproducts.entities.CategoryRealm
import ch.swissbytes.syscomappproducts.entities.Namable
import ch.swissbytes.syscomappproducts.entities.ProductFeatureRealm
import ch.swissbytes.syscomappproducts.entities.ProductRealm
import com.google.android.material.chip.Chip
import java.math.BigDecimal

@BindingAdapter("numMax")
fun numberEditText(tv: TextView, amount: BigDecimal = BigDecimal(100L)) =
    tv.addTextChangedListener(NumberConstraintTextWatcher(minNum = BigDecimal.ZERO, maxNum = amount))


@BindingAdapter("bigDecimalFormat")
fun bigDecimalFormat(tv: TextView, amount: String?) {
    bigDecimalFormat(tv, if (amount == null) null else BigDecimal(amount))
}

@BindingAdapter("bigDecimalFormat")
fun bigDecimalFormat(tv: TextView, amount: BigDecimal?) {
    tv.setText(amount.toFormattedString())
}

@BindingAdapter("productAdapter", "adapterListener")
fun productAdapter(rv: RecyclerView, product: List<ProductRealm>, listener: (id: Namable) -> Unit) {
    rv.adapter = ProductAdapter(listener, product)
}

@BindingAdapter("productFeatureAdapter", "adapterListener")
fun productFeatureAdapter(rv: RecyclerView, productFeature: List<ProductFeatureRealm>, listener: (id: Namable) -> Unit) {
    rv.adapter = NamableAdapter(listener, productFeature, R.layout.horizontal_row)
}

@BindingAdapter("classificationAdapter", "breadCrumbClick", "breadCrumbBackNavigation")
fun categoriesFeatureAdapter(rv: RecyclerView, productFeature: List<CategoryRealm>,
                             onBreadCrumbClick: (id: Namable) -> Unit,
                             onBreadCrumbBackNavigation: (id: Namable) -> Unit) {
    rv.adapter = NamableAdapter(onBreadCrumbClick, productFeature, R.layout.horizontal_row,
            isBreadCrumb = true,
            onBreadCrumbBackNavigation = onBreadCrumbBackNavigation)
}

@BindingAdapter("chipAdapter", "adapterListener")
fun namableAdapter(rv: RecyclerView, namables: List<Namable>, listener: (id: Namable) -> Unit) {
    rv.adapter = NamableAdapter(listener, namables, R.layout.horizontal_chip_row)
}

class NamableAdapter(
    private val listener: GenericListener<Namable>,
    private val items: List<Namable>,
    @LayoutRes val layout: Int = R.layout.row,
    val isBreadCrumb: Boolean = false,
    val onBreadCrumbBackNavigation: GenericListener<Namable>? = null
): RecyclerView.Adapter<NamableAdapter.ViewHolder>() {

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(layout, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        holder.tv?.text = item.name?.toUpperCase()
        if (isBreadCrumb && !item.isLeaf()) holder.chev?.visibility = View.VISIBLE
        holder.itemView.setOnClickListener { onClick(item, position) }
        when (holder.tv) {is Chip -> holder.tv.setOnCloseIconClickListener { onClick(item, position) } }
    }

    private fun onClick(item: Namable, position: Int){
        if (isBreadCrumb && position < itemCount - 1){ onBreadCrumbBackNavigation?.invoke(item) }
        else { listener.invoke(item) }
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val tv = view.findViewById<TextView>(R.id.name)
        val chev = view.findViewById<View?>(R.id.chevron)
    }
}

class ProductAdapter(
    private val listener: GenericListener<Namable>,
    private val items: List<ProductRealm>
): RecyclerView.Adapter<ProductAdapter.ViewHolder>() {

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder =
        ViewHolder(LayoutInflater.from(parent.context).inflate(android.R.layout.simple_list_item_1, parent, false))


    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]
        with(holder){
            tv1.setHtmlText("<small>${item.code}</small> - ${item.name}")
            itemView.setOnClickListener {
                listener.invoke(item)
            }
        }
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        val tv1 = view.findViewById<TextView>(android.R.id.text1)
    }
}