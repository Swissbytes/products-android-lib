package ch.swissbytes.syscomappproducts.realm

import ch.swissbytes.syscomappproducts.realm.module.RealmProductsModule
import io.realm.DynamicRealm
import io.realm.RealmConfiguration
import io.realm.RealmMigration


private val realmMigration: RealmMigration = object: RealmMigration {
    override fun migrate(realm: DynamicRealm, oldV: Long, newVersion: Long) {
        var oldVersion = oldV
        val schema = realm.schema

        /**
         * Realm migration version 3
         */
        if (oldVersion == 2L) {
            schema.create("PriceListDetailRealm")
                .addField("id", Long::class.java)
                ?.addPrimaryKey("id")
                ?.addField("priceListId", Long::class.java)
                ?.setNullable("priceListId", true)
                ?.addField("priority", Long::class.java)
                ?.setNullable("priority", true)

            schema.get("CustomerRealm")
                ?.addRealmListField("priceLists", schema.get("PriceListDetailRealm")!!)

            schema.get("ProductPriceRealm")
                ?.addField("priceListId", Long::class.java)
                ?.setNullable("priceListId", true)

            // PriceListProductPriceRealm
            schema.create("PriceListProductPriceRealm")
                .addField("productId", Long::class.java)
                ?.addPrimaryKey("productId")
                ?.addField("price", String::class.java)
                ?.setNullable("price", true)

            // PriceListPartyRealm
            schema.create("PriceListPartyRealm")
                .addField("partyId", Long::class.java)
                ?.addPrimaryKey("partyId")
                ?.addField("priority", Long::class.java)
                ?.setNullable("priority", true)

            // ProductPriceListRealm
            schema.create("ProductPriceListRealm")
                .addField("id", Long::class.java)
                ?.setNullable("id", true)
                ?.addPrimaryKey("id")
                // companyId
                ?.addField("companyId", Long::class.java)
                ?.setNullable("companyId", true)
                // currency
                ?.addField("currency", String::class.java)
                ?.setNullable("companyId", true)
                // default
                ?.addField("default", Boolean::class.java)
                ?.setNullable("default", true)
                // enablePricing
                ?.addField("enablePricing", Boolean::class.java)
                ?.setNullable("enablePricing", true)
                // from
                ?.addField("from", Long::class.java)
                ?.setNullable("from", true)
                // to
                ?.addField("to", Long::class.java)
                ?.setNullable("to", true)
                // name
                ?.addField("name", String::class.java)
                ?.setNullable("name", true)
                // parties
                ?.addRealmListField("parties", schema.get("PriceListPartyRealm")!!)
                // productPrices
                ?.addRealmListField("productPrices", schema.get("PriceListProductPriceRealm")!!)
                // status
                ?.addField("status", String::class.java)
                ?.setNullable("status", true)

            oldVersion++
        }

        if (oldVersion == 3L) {
            schema.get("ProductStockRealm")!!
                .removeField("reserved")
                .addField("unitMeasureId", Long::class.java)
                .setNullable("unitMeasureId", true)

                // Change available type from Long to String
                .addField("temp_available", String::class.java)
                .transform { obj -> obj.setString("temp_available", obj.getLong("available").toString()) }
                .removeField("available")
                .renameField("temp_available", "available")

                // Change qty type from Long to String
                .addField("temp_qty", String::class.java)
                .transform { obj -> obj.setString("temp_qty", obj.getLong("qty").toString()) }
                .removeField("qty")
                .renameField("temp_qty", "qty")

        }

    }
}

class RealmProductConf  {
   companion object {
       val config: RealmConfiguration by lazy {
           RealmConfiguration.Builder()
               .name("ch.swissbytes.syscomappproducts.realm")
               .modules(RealmProductsModule())
               .schemaVersion(4)
               .migration(realmMigration)
               .build()
       }
   }
}