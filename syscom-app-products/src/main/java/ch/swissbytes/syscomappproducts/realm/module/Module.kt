package ch.swissbytes.syscomappproducts.realm.module

import io.realm.annotations.RealmModule

@RealmModule(library = true, allClasses = true)
class RealmProductsModule