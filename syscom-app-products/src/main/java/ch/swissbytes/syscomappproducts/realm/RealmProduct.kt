package ch.swissbytes.syscomappproducts.realm

import io.realm.Realm

class RealmProduct {
    companion object {
       @JvmStatic fun getDefaultInstance(): Realm = Realm.getInstance(RealmProductConf.config)
    }
}
