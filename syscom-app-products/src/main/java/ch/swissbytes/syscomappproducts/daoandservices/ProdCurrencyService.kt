package ch.swissbytes.syscomappproducts.daoandservices

import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappproducts.base.extensions.realmGetItemCopy
import ch.swissbytes.syscomappproducts.entities.pricing.ProdCurrencyRealm
import ch.swissbytes.syscomappproducts.realm.RealmProduct
import java.math.BigDecimal
import java.math.RoundingMode

class ProdCurrencyService(
    private val exchangeRate: BigDecimal
) {

    fun USDtoBOB(number: BigDecimal): BigDecimal = number.multiply(this.exchangeRate).setScale(2, RoundingMode.HALF_UP)

    fun BOBtoUSD(number: BigDecimal?): BigDecimal {
        val aux = number ?: BigDecimal.ONE
        return aux divideDefaultRounding exchangeRate
    }

    private infix fun BigDecimal.divideDefaultRounding(value: BigDecimal): BigDecimal = this.divide(value, 12, RoundingMode.HALF_UP)
    private infix fun BigDecimal.divideNoRounding(value: BigDecimal): BigDecimal = this.divide(value, 10, RoundingMode.UP)

    fun BOBtoUSDNoRounding(number: BigDecimal?): BigDecimal {
        val aux = number ?: BigDecimal.ONE
        return aux divideNoRounding  exchangeRate
    }

}

fun getGlobalCurrencyService(
    onExchangeRateNotFound: GenericListener<Double>? = null
):ProdCurrencyService {
    val quote = realmGetItemCopy(ProdCurrencyRealm::class.java, RealmProduct.getDefaultInstance()) { this }?.quote

    return if (quote != null){
        ProdCurrencyService(BigDecimal(quote)) //quote
    } else {
        val defaultValue = 6.89
        onExchangeRateNotFound?.invoke(defaultValue)
        ProdCurrencyService(BigDecimal(defaultValue))
    }
}