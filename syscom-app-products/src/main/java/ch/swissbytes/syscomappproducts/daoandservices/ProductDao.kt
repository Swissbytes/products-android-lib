package ch.swissbytes.syscomappproducts.daoandservices

import android.util.Log
import ch.swissbytes.syscomappbase.listeners.GenericListener
import ch.swissbytes.syscomappproducts.BuildConfig
import ch.swissbytes.syscomappproducts.entities.ProductFeatureRealm
import ch.swissbytes.syscomappproducts.entities.ProductFeatureValueRealm
import ch.swissbytes.syscomappproducts.entities.ProductRealm
import ch.swissbytes.syscomappproducts.realm.RealmProduct
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.RealmQuery

data class SearchRequest(
        val products: List<ProductRealm>,
        val productFeature: List<ProductFeatureRealm>,
        val productFeatureValue: List<ProductFeatureValueRealm>
)


interface ProdSearchInterface {
    fun getProductListAsync(localListener: GenericListener<ProductSearchCache>)
}


data class ProductSearchCache(
        var unfilteredListOfProducts: List<ProductRealm>,
        var productFeatureValues: List<ProductFeatureValueRealm>,
        var productFeatures: List<ProductFeatureRealm>
)

/**
 * The base class for the search query performed by
 * [ch.swissbytes.syscomappproducts.activity.products.ProductsActivity.getProductDaoImplementation]
 */
class ProductDao(
        val cache: ProductSearchCache,
        val baseCategory: Long,
        val debug: Boolean = false
) {

    private val TAG = this::class.java.simpleName

    /**
     * Call getSearchRequest asynchronously
     */
    fun getSearchRequestAsync(categoryId: Long, featureValuesSelected: List<List<Long>>) : Observable<SearchRequest> {
        if (BuildConfig.DEBUG) Log.i(TAG, "getSearchRequestAsync called on ${Thread.currentThread().id}")

        return Observable
                .defer { Observable.just(getSearchRequest(categoryId, featureValuesSelected)) }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
    }

    /**
     * @param categoryId the current category selected
     * @param featureValuesSelected an list of list of productFeatureValue (ej: [[blue, green], [large, small]])
     * representing the tags selected
     *
     * @return [SearchRequest] containing:
     * <ul>
     *     <li>The products which path [ProductRealm.PATH] contains the categoryId</li>
     *     <li>The featureValues available based on the new products</li>
     *     <li>The features available based on the featureValues</li>
     * </ul>
     */
    private fun getSearchRequest(
            categoryId: Long,
            featureValuesSelected: List<List<Long>>
    ): SearchRequest {
        if (BuildConfig.DEBUG) Log.i(TAG, "getSearchRequestAsync called inside ${Thread.currentThread().id}")

        val prods = filterByCategoryAndCharacteristics(categoryId, featureValuesSelected)

        var start = System.currentTimeMillis()
        val featureValues = findProductFeaturesValuesFromProducts(prods)
        var end = System.currentTimeMillis()
        Log.i(TAG, "featureValues took: ${end - start} mms")

        start = System.currentTimeMillis()
        val features = findProductFeaturesFromProductsFeatureValues(featureValues)
        end = System.currentTimeMillis()
        Log.i(TAG, "features took: ${end - start} mms")

        return SearchRequest(prods, features, featureValues)
    }


    private fun filterByCategoryAndCharacteristics(
            category: Long,
            characteristics: List<List<Long>>
    ): List<ProductRealm> {
        var prods = cache.unfilteredListOfProducts
//      We filter the list by category only if the user is not in the base category
        if (category != baseCategory){
            prods = prods.filter { it.path.firstOrNull { path -> path.value == category } != null }
//          query.equalTo(ProductRealm.PATH, category)
        }


//        prods.filter { it.characteristics. }
        characteristics.forEachIndexed { i, charas ->
//            if (i > 0) query.and()
//            query.`in`(ProductRealm.CHARACTERISTICS, charas.toTypedArray())
            prods = prods.filter { it.characteristics.map { it.value }.containsAll(charas) }
        }

        return prods
    }



    private fun findProductFeaturesValuesFromProducts(products: List<ProductRealm>): List<ProductFeatureValueRealm> {


        if (products.isEmpty()) return emptyList()

        if (cache.unfilteredListOfProducts.size == products.size) return cache.productFeatureValues

//        cache.productFeatureValues.filter {  }

        RealmProduct.getDefaultInstance().use {
            val query: RealmQuery<ProductFeatureValueRealm> =  it.where(ProductFeatureValueRealm::class.java)
            for (prod in products){
                prod.characteristics
                        .distinctBy { it.value }
                        .forEachIndexed { i, chara ->
                                if (i != 0) query.or()
                                query.equalTo(ProductFeatureValueRealm.ID, chara.value) }
            }
            val list = query.findAll().sort(ProductFeatureValueRealm.NAME)
            return if (list.size > 0) it.copyFromRealm(list) else emptyList()
        }
    }

    private fun findProductFeaturesFromProductsFeatureValues(prodFeatureValues: List<ProductFeatureValueRealm> ): List<ProductFeatureRealm> {
        if (prodFeatureValues.isEmpty()) return emptyList()

        if (prodFeatureValues.size == cache.productFeatureValues.size) return cache.productFeatures

        val list = prodFeatureValues.filter { it.productFeature?.status == "ENABLE" }.map { it.productFeature!! }
        return list.distinctBy { it.id } .sortedBy { it.name }
    }

}