package ch.swissbytes.syscomappproducts.daoandservices

import android.animation.Animator
import android.content.Context
import android.graphics.Rect
import android.os.Handler
import android.util.AttributeSet
import android.view.View
import android.view.ViewAnimationUtils
import android.view.WindowManager
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatEditText
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import ch.swissbytes.syscomappproducts.R
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout

class SearchToolBar {

    private var isSearchOpen: Boolean = false

    var onClose: (() -> Unit)? = null
    var onOpen: (() -> Unit)? = null

    var color: Int? = null

    var activity: AppCompatActivity? = null
        set(value) {
            field = value
            field?.findViewById<View>(R.id.closeContextMenu)?.setOnClickListener { closeContextMenuToolbar() }
            color = activity?.window?.statusBarColor!!
        }


    fun openContextMenuToolbar() {
        if (!isSearchOpen) {
            isSearchOpen = true
            setExpanded(false)

            revealInTooBar()

            activity?.apply {
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                window.statusBarColor = ContextCompat.getColor(this, android.R.color.black)
                findViewById<EditText>(R.id.searchEditText).requestFocus()
                onOpen?.invoke()
            }
        }
    }

    private fun setExpanded(value: Boolean){
        val apl = activity!!.findViewById<AppBarLayout>(R.id.appBarLayout)
        if (!value){
            preventTouchForSecond(600);
            Handler().postDelayed({ this.preventToolBarExpanding() }, 500) } else { allowToolbarExpanding() }
        apl.setExpanded(value, true)
    }

    fun closeContextMenuToolbar(): Boolean {
        if (isSearchOpen) {
            isSearchOpen = false
            activity?.apply {
                setExpanded(true)
                revealOutToolBar()
                color?.let { window.statusBarColor = it }
                setTheme(R.style.NoTranslucentStatus)
                onClose?.invoke()
            }
            return true
        }
        return false
    }

    private fun preventToolBarExpanding() {
        activity?.apply {
            val ctb = findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout)
            val p = ctb.layoutParams as AppBarLayout.LayoutParams
            p.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SNAP
            findViewById<AppBarLayout>(R.id.appBarLayout).setVisibility(View.GONE)
        }
    }

    private fun allowToolbarExpanding() {
        activity?.apply {
            val ctb = findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout)
            val p = ctb.layoutParams as AppBarLayout.LayoutParams
            p.scrollFlags = AppBarLayout.LayoutParams.SCROLL_FLAG_SCROLL or AppBarLayout.LayoutParams.SCROLL_FLAG_ENTER_ALWAYS
            findViewById<AppBarLayout>(R.id.appBarLayout).visibility = View.VISIBLE
        }
    }

    private fun preventTouchForSecond(sec: Int) {
        activity?.apply {
            window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
            Handler().postDelayed({ window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE) }, sec.toLong())
        }
    }

    private fun revealOutToolBar() {
        activity?.findViewById<Toolbar>(R.id.search_toolbar)?.run { UiUtils.revealOut(this, width - 110, height / 2, 300) }
        activity?.findViewById<View>(R.id.fake_toolbar)?.run { visibility = View.GONE }
    }

    private fun revealInTooBar() {
        activity?.findViewById<Toolbar>(R.id.search_toolbar)?.run { UiUtils.revealIn(this, width - 110, height / 2, 300) }
        activity?.findViewById<View>(R.id.fake_toolbar)?.run { visibility = View.VISIBLE }
    }

}

object UiUtils {

    fun revealIn(shape: View, xPos: Int, yPos: Int, duration: Int) {
        val circularReveal = ViewAnimationUtils.createCircularReveal(shape, xPos, yPos, 0f, Math.hypot(shape.width.toDouble(), shape.height.toDouble()).toFloat())
        circularReveal.interpolator = AccelerateDecelerateInterpolator()
        circularReveal.duration = duration.toLong()
        circularReveal.start()
        shape.visibility = View.VISIBLE
    }

    fun revealOut(shape: View, xPos: Int, yPos: Int, duration: Long) {
        val finalRadius = Math.max(shape.width, shape.height)
        val animator = ViewAnimationUtils.createCircularReveal(shape, xPos, yPos, finalRadius.toFloat(), 0f)
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.duration = duration
        animator.start()
        animator.addListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {

            }

            override fun onAnimationEnd(animation: Animator) {
                shape.visibility = View.INVISIBLE
            }

            override fun onAnimationCancel(animation: Animator) {

            }

            override fun onAnimationRepeat(animation: Animator) {

            }
        })
    }

}

class FocusEditText : AppCompatEditText {

    var delay = 0
    internal var context: Context

    constructor(context: Context) : super(context) {
        this.context = context
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        this.context = context
        val a = context.theme.obtainStyledAttributes(attrs, R.styleable.FocusEditText, 0, 0)

        try {
            delay = a.getInteger(R.styleable.FocusEditText_focus_delay_mms, 0)
        } finally {
            a.recycle()
        }
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        this.context = context
    }

    override fun onFocusChanged(focused: Boolean, direction: Int, previouslyFocusedRect: Rect?) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect)
        if (focused) {
            android.os.Handler().postDelayed({
                val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
            }, delay.toLong())
        } else {
            val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(this.windowToken, 0)
        }
    }
}