package ch.swissbytes.syscomappproducts.daoandservices

import ch.swissbytes.syscomappproducts.base.extensions.realmGetItemCopy
import ch.swissbytes.syscomappproducts.entities.pricing.CustomerRealm
import ch.swissbytes.syscomappproducts.realm.RealmProduct

fun getCustomerCopyById(customerId: String) =
        realmGetItemCopy(CustomerRealm::class.java, realm = RealmProduct.getDefaultInstance())
        { equalTo("id", customerId) } ?: realmGetItemCopy(CustomerRealm::class.java, realm = RealmProduct.getDefaultInstance())
        { equalTo("originalAppUuid", customerId) }

fun getCustomerCopyById(customerId: Long) = getCustomerCopyById(customerId.toString())