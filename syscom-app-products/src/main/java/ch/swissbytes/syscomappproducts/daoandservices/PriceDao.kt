package ch.swissbytes.syscomappproducts.daoandservices

import android.util.Log
import ch.swissbytes.syscomappproducts.base.extensions.realmUse
import ch.swissbytes.syscomappproducts.base.extensions.toLongOrZero
import ch.swissbytes.syscomappproducts.entities.ProductPriceRealm
import ch.swissbytes.syscomappproducts.entities.prodPrice.ProductPriceListRealm
import ch.swissbytes.syscomappproducts.realm.RealmProduct
import io.realm.Realm
import java.math.BigDecimal
import java.util.*


class PriceDao(
    private val isPriceListLegacyModeOn: Boolean,
    private val defaultPriceListId: Long
) {

    private val TAG = this::class.java.simpleName

    /**
     * customerId as String should be consider as id zero, which will set the customer entity as null
     * and finally set the default price list, since an unregistred customer don't have any priceRules set.
     */
    fun getProductPriceWithCustomerIdString(
        productId: Long,
        businessUnitId: Long,
        customerId: String
    ): BigDecimal = getProductPrice(productId, businessUnitId, customerId.toLongOrZero())

    fun getProductPrice(
        productId: Long,
        businessUnitId: Long,
        customerId: Long
    ): BigDecimal {
        var ppl: BigDecimal = BigDecimal.ZERO

        realmUse(RealmProduct.getDefaultInstance()) {
            ppl = getProductPriceWithRealm(this, productId, businessUnitId, customerId)
        }

        Log.d("TAG", "product price $ppl found for productId: $productId in bId: $businessUnitId")
        return ppl
    }

    fun getProductPriceWithRealm(
        realm: Realm,
        productId: Long,
        businessUnitId: Long,
        customerId: Long
    ) = if (!isPriceListLegacyModeOn) getProductPriceByPriceList(realm, productId , customerId)
        else getProductPriceNoPriceList(realm, productId, businessUnitId)

    /**
     * Get the product price based on productPriceList LegacyMode
     */
    private fun getProductPriceByPriceList(
        realm: Realm,
        productId: Long,
        customerId: Long
    ): BigDecimal {
        val customer = getCustomerCopyById(customerId)


        var priceListId = customer?.findPriceListWithGreaterPriorityByPriceListId()?.priceListId
        if (priceListId == null){
            priceListId = defaultPriceListId
            Log.w(TAG, "No price list found for customer ${customer?.name} with id ${customer?.id} we set up the defaultPriceListId ")
        }

//        Log.i(TAG, " priceListId $priceListId found for customerId ${customer?.id} productId $productId")

        val now = Date().time

        val productPriceList = realm.where(ProductPriceListRealm::class.java)
                .equalTo("id", priceListId)
                .equalTo("status", "ENABLE")
                .equalTo("productPrices.productId", productId)
                .beginGroup()
                .greaterThanOrEqualTo("from", now)
                .and().lessThanOrEqualTo("to", now)
                .or().isNull("to")
                .endGroup()
                .findFirst()

        var price = productPriceList
            ?.productPrices
            ?.firstOrNull { it.productId == productId }
            ?.price
            ?.toBigDecimalOrNull() ?: BigDecimal.ZERO

        if (productPriceList?.currency == "USD") {
            price = getGlobalCurrencyService().USDtoBOB(price)
        }

        return price
    }

    /**
     * Get the product price based on businessUnitId
     */
    private fun getProductPriceNoPriceList(
        realm: Realm,
        productId: Long,
        businessUnitId: Long
    ): BigDecimal {
        return realm.where(ProductPriceRealm::class.java)
            .equalTo(ProductPriceRealm.B_ID, businessUnitId).and()
            .equalTo(ProductPriceRealm.P_ID, productId)
            .findFirst()?.price?.toBigDecimal() ?: BigDecimal.ZERO
    }

}