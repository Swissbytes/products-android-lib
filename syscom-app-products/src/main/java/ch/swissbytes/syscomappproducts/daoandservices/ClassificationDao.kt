package ch.swissbytes.syscomappproducts.daoandservices

import ch.swissbytes.syscomappproducts.entities.CategoryRealm
import ch.swissbytes.syscomappproducts.realm.RealmProduct
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import io.realm.RealmResults


class ClassificationDao {

    fun getClassificationChildOfAsync(parentId: Long): Observable<List<CategoryRealm>> {
        return  Observable.defer { Observable.just(getClassificationChildOf(parentId)) }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
    }

    private fun getClassificationChildOf(parentId: Long): List<CategoryRealm> {
        RealmProduct.getDefaultInstance().use {
            val result: RealmResults<CategoryRealm> =  it.where(CategoryRealm::class.java).equalTo(CategoryRealm.PARENT_ID, parentId).findAll()
            return if (result.isEmpty()) emptyList() else it.copyFromRealm(result)
        }
    }

    fun getById(id: Long): CategoryRealm? {
        RealmProduct.getDefaultInstance().use {
            val result: CategoryRealm? =  it.where(CategoryRealm::class.java).equalTo(CategoryRealm.ID, id).findFirst()
            return if (result == null) null else it.copyFromRealm(result)
        }
    }

}