package ch.swissbytes.syscomappproducts.fragments

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.DecelerateInterpolator
import android.view.animation.Transformation
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.Guideline
import androidx.fragment.app.Fragment
import ch.swissbytes.syscomappbase.extensions.addFragment
import ch.swissbytes.syscomappbase.extensions.getHasMutableList
import ch.swissbytes.syscomappbase.listeners.SimpleListener
import ch.swissbytes.syscomappproducts.R
import ch.swissbytes.syscomappproducts.activity.products.ProductsActivity
import ch.swissbytes.syscomappproducts.activity.products.fragments.ProductSelectedFragment
import ch.swissbytes.syscomappproducts.entities.SaleProductParcelable


class ProductsTabFragment : Fragment() {

    private val TAG = this::class.java.simpleName

    var productSelectedFragment: ProductSelectedFragment? = null
    var undo: View? = null
    var onCurrentSelectionChange: ((MutableList<SaleProductParcelable>) -> Unit)? = null
    var onForceActivityQuitByDirectSale: (SimpleListener)? = null
    var onRestoreDelayFinish: ((Boolean) -> Unit)? = null
    private var guideline: Guideline? = null

    private var currentProductSelection = mutableListOf<SaleProductParcelable>()
        get() = productSelectedFragment?.currentProductSelection ?: mutableListOf()

    private var baseCategoryId: Long? = null
    private var isEditable: Boolean = false
    private var constraintToStock: Boolean = false
    private var canOverflowAvailableStock: Boolean = false
    private var products: ArrayList<SaleProductParcelable>? = null

    var productActivityRedirection: ProductsActivity? = null

    private var businessUnitId: Long? = null
    var warehouseId: Long? = null
    var customerId: String? = null
    var priceListId: Long? = null
    var isPriceListLegacyModeOn: Boolean = false


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            businessUnitId = it.getLong(ARG_B_ID)
            warehouseId = it.getLong(ARG_W_ID)
            customerId = it.getString(ARG_C_ID)
            baseCategoryId = it.getLong(ARG_BASE_CATEGORY_ID)
            isPriceListLegacyModeOn = it.getBoolean(ARG_PRICE_LIST_LEGACY_MODE)
            priceListId = it.getLong(ARG_PRICE_LIST_ID)
            isEditable = it.getBoolean(ARG_IS_EDITABLE)
            constraintToStock = it.getBoolean(ARG_CONSTRAINT_TO_STOCK)
            canOverflowAvailableStock = it.getBoolean(ARG_CAN_OVERFLOW_AVAILABLE_STOCK)
            products = it.getParcelableArrayList(ARG_PRODUCTS_FRAGMENT)

            if (it.getBoolean(ARG_REDIRECT_FOR_DIRECT_SALE)){
                onEditOrderCLick(true)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_prod_order, container, false)
        guideline =  v.findViewById(R.id.guideline)

        productSelectedFragment = ProductSelectedFragment
            .newInstance(
                list = products!!,
                businessUnitId = businessUnitId!!,
                warehouseId = warehouseId!!,
                customerId = customerId!!,
                canBeModified = isEditable,
                constraintToStock = constraintToStock,
                canOverflowAvailableStock = canOverflowAvailableStock,
                isPriceListLegacyModeOn = isPriceListLegacyModeOn,
                defaultPriceListId = priceListId ?: 0
            ).apply {
                trashClick = {
                    showUndoBtn(true)
                }
                restoreDelayFinish = {
                    showUndoBtn(false);
                    onRestoreDelayFinish?.invoke(it)
                }
                onCurrentSelectionChange = { currentProductSelectionChange(it) }
                beforeProductAdded = { _, _, c -> c.invoke() }
                beforeProductRemoved = { _, _, c -> c.invoke() }
            }
        (activity as AppCompatActivity).addFragment(productSelectedFragment!!, R.id.fragment_container)
        return v
    }

    fun updateBusinessUnitId(value : Long?){
        businessUnitId = value
        productSelectedFragment?.updateBusinessUnitId(value)
    }

    fun updateWarehouseId(value: Long?){
        warehouseId = value
        productSelectedFragment?.updateWarehouseId(value)
    }

    fun updateCustomerId(value: String?){
        customerId = value
        productSelectedFragment?.updateCustomerId(value)
    }

    fun updatePriceListIdChange(lPriceListId: Long) {
        priceListId = lPriceListId
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (!isEditable) return

        view.findViewById<View>(R.id.btn)?.apply {
            visibility = View.VISIBLE
            setOnClickListener { onEditOrderCLick() }
        }

        view.findViewById<View>(R.id.btn_undo).apply {
            setOnClickListener{ productSelectedFragment?.restore(); showUndoBtn(false) }; undo = this
        }
    }

    fun forceEmptyTrash(){
        productSelectedFragment?.forceEmptyTrash()
    }

    //Called by the ProductSelected fragment and after a response from the Product Activity
    private fun currentProductSelectionChange(p: MutableList<SaleProductParcelable>){
        onCurrentSelectionChange?.invoke(p)
    }

    private var isUndoShowing = false
    private val UNDO_WIDTH = 360F
    private fun showUndoBtn(show: Boolean){
        val animShouldRun = !((show && isUndoShowing) || (!show && !isUndoShowing))
        if (animShouldRun){
            object: Animation() {
                override fun applyTransformation(interpolatedTime: Float, t: Transformation?) {
                    val value = if (show) (UNDO_WIDTH * interpolatedTime).toInt()
                    else  (UNDO_WIDTH * Math.abs(interpolatedTime -1f)).toInt()

                    guideline?.setGuidelineEnd(value) }
                }.apply {
                    interpolator = DecelerateInterpolator()
                    fillAfter = true
                    duration = if (show) 500L else 200L // in ms
                    guideline!!.startAnimation(this)
            }
        }
        isUndoShowing = show
    }

    fun onEditOrderCLick(showConfirmMenu: Boolean? = false) {
        productSelectedFragment?.forceEmptyTrash()

        if (productActivityRedirection == null){
            throw IllegalStateException("productActivityRedirection is null")
        }

        val products = ArrayList(currentProductSelection)
        Intent(context, productActivityRedirection!!::class.java).apply {
            putExtra(ProductsActivity.INPUT, products)
            putExtra(ProductsActivity.STOCK_BALANCE, ArrayList(productSelectedFragment?.stockBalance() ?: emptyList()))
            putExtra(ProductsActivity.B_ID, businessUnitId)
            putExtra(ProductsActivity.W_ID, warehouseId)
            putExtra(ProductsActivity.C_ID, customerId)
            putExtra(ProductsActivity.PRICE_LIST_ID, priceListId)
            putExtra(ProductsActivity.PRICE_LIST_LEGACY_MODE, isPriceListLegacyModeOn)
            putExtra(ProductsActivity.CONSTRAINT_TO_STOCK, constraintToStock)
            putExtra(ProductsActivity.ALLOW_BACK_ARROW, showConfirmMenu)
            putExtra(ProductsActivity.CATEGORY_ID, baseCategoryId)
            putExtra(ProductsActivity.CAN_OVERFLOW_AVAILABLE_STOCK, canOverflowAvailableStock)
            startActivityForResult(this, 1)
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1){
            Log.i(TAG, "onActivityResult: $data")
            if (resultCode == Activity.RESULT_OK){
                val result: MutableList<SaleProductParcelable> = data.getHasMutableList("result")
                currentProductSelectionChange(result)   //Notify the changes out of the library
                updateProductList(result)               //Notify the changes inside the library
            }

            if (resultCode == ProductsActivity.DIRECT_SALE_QUIT){
                onForceActivityQuitByDirectSale?.invoke()
            }
        }
    }

    fun updateProductList(result: MutableList<SaleProductParcelable>){
        productSelectedFragment?.updateProductList(result)
    }

    companion object {
        val ARG_PRODUCTS_FRAGMENT = "ARG_PRODUCTS_FRAGMENT"
        val ARG_B_ID = "ARG_B_ID"
        val ARG_W_ID = "ARG_W_ID"
        val ARG_C_ID = "ARG_C_ID"
        val ARG_CONSTRAINT_TO_STOCK = "ARG_CONSTRAINT_TO_STOCK"
        val ARG_IS_EDITABLE = "ARG_IS_EDITABLE"
        val ARG_BASE_CATEGORY_ID = "ARG_BASE_CATEGORY_ID"
        val ARG_PRICE_LIST_ID = "ARG_PRICE_LIST_ID"
        val ARG_PRICE_LIST_LEGACY_MODE = "ARG_PRICE_LIST_LEGACY_MODE"
        val ARG_CAN_OVERFLOW_AVAILABLE_STOCK = "ARG_CAN_OVERFLOW_AVAILABLE_STOCK"
        val ARG_REDIRECT_FOR_DIRECT_SALE = "ARG_REDIRECT_FOR_DIRECT_SALE"

        /**
         * @param isEditable show add product button on top of the fragment layout
         * @param canOverflowAvailableStock if false don't allow product modal dialog to pop up
         * @param constraintToStock The constraint put on the product modal dialog
         */
        @JvmStatic fun newInstance(
            list: ArrayList<SaleProductParcelable>,
            baseCategoryId: Long,
            businessUnitId: Long,
            warehouseId: Long,
            customerId: String,
            constraintToStock: Boolean = false,
            isEditable:Boolean = false,
            redirectForDirectSale:Boolean = false,
            canOverflowAvailableStock:Boolean = false,
            isPriceListLegacyModeOn:Boolean = false,
            defaultPriceListId:Long
        ) = ProductsTabFragment().apply {
            arguments = Bundle().apply {
                putParcelableArrayList(ARG_PRODUCTS_FRAGMENT, list)
                putLong(ARG_B_ID, businessUnitId)
                putLong(ARG_W_ID, warehouseId)
                putString(ARG_C_ID, customerId)
                putBoolean(ARG_IS_EDITABLE, isEditable)
                putBoolean(ARG_CONSTRAINT_TO_STOCK, constraintToStock)
                putBoolean(ARG_PRICE_LIST_LEGACY_MODE, isPriceListLegacyModeOn)
                putLong(ARG_PRICE_LIST_ID, defaultPriceListId)
                putLong(ARG_BASE_CATEGORY_ID, baseCategoryId)
                putBoolean(ARG_REDIRECT_FOR_DIRECT_SALE, redirectForDirectSale)
                putBoolean(ARG_CAN_OVERFLOW_AVAILABLE_STOCK, canOverflowAvailableStock)
            } }
    }

}