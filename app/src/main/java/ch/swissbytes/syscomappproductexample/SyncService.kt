package ch.swissbytes.syscomappproductexample

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.util.Log
import ch.swissbytes.syscomappbase.appspecific.Roles
import ch.swissbytes.syscomappbase.appspecific.SyncInterface
import ch.swissbytes.syscomappbase.interfaces.SyscomSyncInterface

import ch.swissbytes.syscomappproducts.services.ProductSync
import java.util.*


class SyncService: Service() {

    private val TAG = this::class.java.simpleName
    override fun onBind(intent: Intent) = LocalBinder()
    inner class LocalBinder : Binder() {  val service = this@SyncService }
    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int  = Service.START_STICKY

    var callsDone: Int = 0; var callsSize: Int = 0; var errorOccure: Boolean = false

    private val _token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJGR09OWkFMRVMiLCJ0b2tlblR5cGUiOiJQQVJUWV9ST0xFIiwidXNlckluZm8iOiJ7XHJcbiAgXCJ1c2VyTmFtZVwiIDogXCJGR09OWkFMRVNcIixcclxuICBcImNvbXBhbnlJZFwiIDogMVxyXG59IiwiaXNzIjoiaHR0cDovLzE5Mi4xNjguMC4xNzk6NTA4MC9yZXN0L21vYmlsZS9zYWxlc0NvbGxlY3Rpb25zVmlzaXRzL2F1dGgiLCJpYXQiOjE1Njg5OTY2NDYsImV4cCI6NDcyMjU5NjY0Nn0.PR6_uU5XDYC6HcEOpUZ7_0Lto5ODd74q4ukXcMTQjD7Krd1EgGWG5DouhrdcrnkJKzAE8UU3mqGnoFB_Kx7EzA"
    private val calls: List<SyscomSyncInterface> by lazy {

        val input = object : SyncInterface {
            override var appId: String = ""
            override var url: String = "http://192.168.0.179:5080/rest/mobile/"
            override var token: String = _token
            override var versionCode: String = ""
            override var timeout: Long = 0
            override var lastUpdate: Long? = null
            override var roles: EnumSet<Roles> = EnumSet.of(Roles.SELLER, Roles.COLLECTOR, Roles.FLEET)
            override var debug: Boolean = true
        }

        listOfNotNull(ProductSync(input)) //10.0.2.2
    }


    fun sync(){
        calls.forEach { callsSize += it.numOfCalls() }
        calls.forEach { call -> call.startSync({ checkAdvance() }) { errorOccure = true } }
    }

    @Synchronized fun checkAdvance(){
        if (callsSize == ++callsDone){
            Log.i(this::class.java.simpleName, "all calls done")
            this.stopSelf()
        }
    }

}
