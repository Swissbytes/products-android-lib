package ch.swissbytes.syscomappproductexample

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import ch.swissbytes.syscomappbase.extensions.getHasMutableList
import ch.swissbytes.syscomappproducts.activity.products.ProductsActivity
import ch.swissbytes.syscomappproducts.entities.SaleProductParcelable
import ch.swissbytes.syscomappproducts.fragments.ProductsTabFragment

class TabActivity : AppCompatActivity() {

    private val TAG = this.javaClass.simpleName

    private var fragment: ProductsTabFragment? = null
    private var currentSelectedProducts = mutableListOf<SaleProductParcelable>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_tab)
        supportFragmentManager.beginTransaction().apply {
            val frag = ProductsTabFragment.newInstance(
                arrayListOf(), 1L, 3L, 2L,
                isEditable = true,
                canOverflowAvailableStock = true,
                customerId = "1",
                defaultPriceListId = 1L,
                isPriceListLegacyModeOn = true
            ).apply {
                productActivityRedirection = MProductActivity::class.java.newInstance()!!
            }

            fragment = frag
            add(R.id.fragment, frag)
            commit()
        }
    }

//    override fun onEditOrderCLick(disableBackArrow: Boolean) {
//        val intent = Intent(this, MProductActivity::class.java)
//        intent.putExtra(ProductsActivity.B_ID, 1L)
//        intent.putExtra(ProductsActivity.W_ID, 3L)
//        intent.putExtra(ProductsActivity.INPUT, ArrayList(currentSelectedProducts))
//        intent.putExtra(ProductsActivity.ALLOW_BACK_ARROW, true)
//        startActivityForResult(intent, 1)
//    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == 1) {
            Log.i(TAG, "onActivityResult: $data")
            if (resultCode == Activity.RESULT_OK) {
                currentSelectedProducts = data.getHasMutableList("result")
                fragment?.updateProductList(currentSelectedProducts)
            }

            if (resultCode == ProductsActivity.DIRECT_SALE_QUIT) {
                this.finish()
            }
        }
    }

}
