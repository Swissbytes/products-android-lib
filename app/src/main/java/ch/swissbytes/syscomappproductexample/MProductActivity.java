package ch.swissbytes.syscomappproductexample;

import org.jetbrains.annotations.NotNull;

import ch.swissbytes.syscomappproducts.activity.products.ProductsActivity;
import ch.swissbytes.syscomappproducts.daoandservices.ProductDao;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;

public class MProductActivity extends ProductsActivity {

    @Override
    public void getProductDaoImplementation(@NotNull Function1<? super ProductDao, Unit> listener) {

    }
}
